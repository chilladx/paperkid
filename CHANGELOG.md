# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- [server] Create DB storage
- [server] Create data crawler
- [server] Create recurring crawling task
- [api] Create gRPC/REST server
- [api] CRUD for feed
- [api] CRUD for articles
- [web] List the feeds
- [web] List the articles per feed
- [web] Read an article
- [*] Package the application in a docker-compose file

## [1.0.0] - YYYY-mm-dd
