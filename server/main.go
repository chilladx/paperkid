package server

import (
	"context"
	"encoding/base64"
	"fmt"
	"math"
	"reflect"
	"strconv"

	"github.com/golang/protobuf/proto"
	logging "github.com/op/go-logging"
	"gitlab.com/chilladx/paperkid/api"
	"gitlab.com/chilladx/paperkid/db"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Create a new instance of the logger
var log = logging.MustGetLogger("main")

const (
	// FeedName is the fix part of the 'name' string
	FeedName = "feeds"
	// ArticleName is the fix part of the 'name' string
	ArticleName = "articles"
)

const (
	defaultPageSize = 20
	maxPageSize     = 100
)

// nextPageToken creates a `next_page_token` for a paginated response from a request, and adds additional data.
// `req` must be a protobuf message containing a `PageToken` field. Returns a URL-safe base64 encoded string.
func nextPageToken(req proto.Message, a ...interface{}) (string, error) {
	log.Debugf("Generating next page token for %s", req)

	req = stripToken(req)

	buf := proto.NewBuffer([]byte{})

	// Write the protobuf message
	err := buf.EncodeMessage(req)
	if err != nil {
		return "", fmt.Errorf("failed to encode request to buffer")
	}

	// Write the extra data
	for _, d := range a {
		switch v := d.(type) {
		case int:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case int8:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case int16:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case int32:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case int64:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case uint:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case uint8:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case uint16:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case uint32:
			if err = buf.EncodeVarint(uint64(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case uint64:
			if err = buf.EncodeVarint(v); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case bool:
			if v {
				if err = buf.EncodeVarint(1); err != nil {
					return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
				}
			} else {
				if err = buf.EncodeVarint(0); err != nil {
					return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
				}
			}
		case float32:
			if err = buf.EncodeFixed32(uint64(math.Float32bits(v))); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case float64:
			if err = buf.EncodeFixed64(math.Float64bits(v)); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case string:
			if err = buf.EncodeStringBytes(v); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case []byte:
			if err = buf.EncodeRawBytes(v); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		case proto.Message:
			if err = buf.EncodeMessage(v); err != nil {
				return "", fmt.Errorf("failed to encode extra data to buffer (%s)", err)
			}
		default:
			return "", fmt.Errorf("can't encode type %T into token", v)
		}
	}

	return base64.RawURLEncoding.EncodeToString(buf.Bytes()), nil
}

// validatePageToken validates a page token against a request. It decodes the given token, verifies it matches the
// request, and stores the extra data into successive arguments. Arguments must be pointers. Behavior is undefined
// if the arguments do not match the encoded buffer. Returns true if the token matches the request.
func validatePageToken(token string, req proto.Message, a ...interface{}) (bool, error) {
	dec, err := base64.RawURLEncoding.DecodeString(token)
	if err != nil {
		return false, err
	}

	req = stripToken(req)

	buf := proto.NewBuffer(dec)

	// Read input and compare
	in := proto.Clone(req)
	in.Reset()
	if err = buf.DecodeMessage(in); err != nil {
		return false, fmt.Errorf("failed to decode message (%s)", err)
	}
	if !proto.Equal(req, in) {
		return false, nil
	}

	// Read extra data
	for _, d := range a {
		switch ptr := d.(type) {
		case *int:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = int(v)
		case *int8:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = int8(v)
		case *int16:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = int16(v)
		case *int32:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = int32(v)
		case *int64:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = int64(v)
		case *uint:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = uint(v)
		case *uint8:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = uint8(v)
		case *uint16:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = uint16(v)
		case *uint32:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = uint32(v)
		case *uint64:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = v
		case *bool:
			v, err := buf.DecodeVarint()
			if err != nil {
				return false, err
			}
			*ptr = (v == 1)
		case *float32:
			v, err := buf.DecodeFixed32()
			if err != nil {
				return false, err
			}
			*ptr = math.Float32frombits(uint32(v))
		case *float64:
			v, err := buf.DecodeFixed64()
			if err != nil {
				return false, err
			}
			*ptr = math.Float64frombits(v)
		case *string:
			v, err := buf.DecodeStringBytes()
			if err != nil {
				return false, err
			}
			*ptr = v
		case []byte:
			v, err := buf.DecodeRawBytes(false)
			if err != nil {
				return false, err
			}
			copy(ptr, v)
		case *[]byte:
			v, err := buf.DecodeRawBytes(true)
			if err != nil {
				return false, err
			}
			*ptr = v
		case proto.Message:
			err := buf.DecodeMessage(ptr)
			if err != nil {
				return false, err
			}
		default:
			if reflect.ValueOf(req).Kind() != reflect.Ptr {
				return false, fmt.Errorf("can't store data into non-pointer type %T", d)
			}
			return false, fmt.Errorf("can't encode type %T into token", d)
		}
	}

	return true, err
}

func stripToken(req proto.Message) proto.Message {
	// Clone the request to avoid overwriting input argument
	req = proto.Clone(req)
	in := reflect.ValueOf(req).Elem()

	// Set PageToken field to empty string
	for i := 0; i < in.NumField(); i++ {
		f := in.Type().Field(i)
		if f.Name == "PageToken" && f.Type == reflect.TypeOf("") {
			in.Field(i).Set(reflect.ValueOf(""))
			return req
		}
	}

	return req
}

func getAuthIDFromContext(ctx context.Context) (int64, error) {
	ai, ok := ctx.Value(accountIDKey).(string)
	if !ok && ai != "" {
		log.Debugf("Can't find Authentication ID in context")
		return 0, status.Errorf(codes.Unauthenticated, "Missing authentication to get account")
	}

	if ai == "" {
		log.Debugf("Unable to find AuthID in the context")
		return 0, nil
	}

	authID, err := strconv.ParseInt(ai, 10, 64)
	if err != nil {
		log.Debugf("Authentication ID stored in context is wrong: %v", ai)
		return 0, status.Errorf(codes.Unauthenticated, "Authentication has wrong format")
	}

	return authID, nil
}

// checkUserRole returns true if the given user (in the context) is part of 'required' group
func checkUserRole(ctx context.Context, pdb db.PaperkidDB, required api.User_Role) (res bool, err error) {
	authID, err := getAuthIDFromContext(ctx)
	if err != nil {
		log.Errorf("can't find AuthId in context")
		return res, fmt.Errorf("unable to get auth_id from context")
	}

	dbUser, err := pdb.GetUserByAuthID(authID)
	if err != nil {
		log.Errorf("can't find user with AuthID %d in DB", authID)
		return res, fmt.Errorf("Unable to get user: %s", err.Error())
	}

	for _, r := range dbUser.Role {
		if api.User_Role(r) == required {
			res = true
			break
		}
	}
	if !res {
		log.Errorf("User with AuthID %d misses role %s to get in", authID, required)
	}

	return res, nil
}

// Contains returns true if a string slice contains the given string, false otherwise
func Contains(l []string, s string) bool {
	for _, i := range l {
		if i == s {
			return true
		}
	}
	return false
}
