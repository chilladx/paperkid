package server

import (
	"bytes"
	"fmt"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"

	"golang.org/x/net/context"

	"github.com/keratin/authn-go/authn"
	"github.com/rs/cors"
	"github.com/spf13/viper"
	"gitlab.com/chilladx/paperkid/api"
	"gitlab.com/chilladx/paperkid/db"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
)

// private type for Context keys
type contextKey int

const (
	accountIDKey contextKey = iota
)

// Configuration contains the parameters to run a gRPC server
type Configuration struct {
	BindAddress string
	BindPort    int
	RestAddress string
	RestPort    int
	Debug       bool
}

// credentialMatcher matches HTTP header and authorizes their injection in gRPC context
func credentialMatcher(headerName string) (mdName string, ok bool) {
	// Match all headers
	return strings.ToLower(headerName), true
}

// identifyUser checks the API token, and tries to identify User from it
func identifyUser(ctx context.Context, _ *Server) (string, error) {
	if err := authn.Configure(authn.Config{
		// The AUTHN_URL of your Keratin AuthN server. This will be used to verify tokens created by
		// AuthN, and will also be used for API calls unless PrivateBaseURL is also set.
		// Issuer: "http://auth.paperkid.news",
		Issuer: viper.GetString("auth.issuer"),

		// The domain of your application (no protocol). This domain should be listed in the APP_DOMAINS
		// of your Keratin AuthN server.
		Audience: viper.GetString("auth.domain"),

		// Credentials for AuthN's private endpoints. These will be used to execute admin actions using
		// the Client provided by this library.
		//
		// TIP: make them extra secure in production!
		Username: viper.GetString("auth.username"),
		Password: viper.GetString("auth.password"),

		// OPTIONAL: Send private API calls to AuthN using private network routing. This can be
		// necessary if your environment has a firewall to limit public endpoints.
		// PrivateBaseURL: "http://localhost:8765",
		PrivateBaseURL: viper.GetString("auth.privateaddress"),
	}); err != nil {
		log.Errorf("unable to configure AuthN: %s", err)
		return "", fmt.Errorf("can't indentify this token")
	}

	if md, ok := metadata.FromIncomingContext(ctx); ok {
		userToken := strings.Join(md["paperkid-token"], "")

		// SubjectFrom will return an AuthN account ID that you can use as to identify the user, if and
		// only if the token is valid.
		sub, err := authn.SubjectFrom(userToken)
		if err != nil {
			log.Errorf("unable to identify user: %s", err)
			log.Errorf("user token: %s", userToken)
			return "", nil
		}

		return sub, nil
	}
	return "", fmt.Errorf("missing credentials")
}

// logInterceptor logs the request after it passes through the handler. The log
// format is similar to the NCSA combined log format, replacing:
//  * the HTTP request line with the gRPC method name
//  * the HTTP return status code with the gRPC return code
//  * the size of the response in bytes with the size of the serialized protobuf message
// Apache's mod_log_config format string:
// "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\""
func logInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (res interface{}, err error) {
	// Store incoming request time
	reqTime := time.Now()

	// Call the actual handler (or next interceptor in the chain)
	res, err = handler(ctx, req)

	var accountID string
	var ok bool
	if accountID, ok = ctx.Value(accountIDKey).(string); ok {
		if accountID == "" {
			accountID = "-"
		}
	} else {
		accountID = "-"
	}
	// Initialize all strings
	addr := "-"                                                 // The remote hostname
	logname := "-"                                              // Always "-", has something to do with identd
	username := accountID                                       // Remote user if the request was authenticated.
	timestamp := reqTime.Format("[02/Jan/2006 03:04:05 -0700]") // Time the request was received
	request := info.FullMethod                                  // The full gRPC method name
	statusCode := "-"                                           // The gRPC return status
	byteSize := "-"                                             // The size of the response in bytes
	referer := "-"                                              // The Referer header, if any
	userAgent := "-"                                            // The User-Agent header, if any

	// Get the remote peer info from the context
	if p, ok := peer.FromContext(ctx); ok {
		if tcpAddr, ok := p.Addr.(*net.TCPAddr); ok {
			addr = tcpAddr.IP.String()
		} else {
			addr = p.Addr.String()
		}
	}

	// TODO: Get the username if logged in

	if err == nil {
		statusCode = codes.OK.String()
		// It is impossible to know the size of a protobuf message without serializing it before
		// and we haven't serialized the message at the interceptor level yet. We could serialize
		// it here, and again before sending it to the client, but it seems like this could be
		// a performance issue. Commenting for now, we *could* have this be a configuration option.
		// if pb, ok := res.(proto.Message); ok {
		// 	if b, err := proto.Marshal(pb); err == nil {
		// 		size = fmt.Sprintf("%d", len(b))
		// 	}
		// }
	} else if grpcErr, ok := status.FromError(err); ok {
		statusCode = grpcErr.Code().String()
		// TODO: get the size of the error
	}

	// Get metadata from context
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		if r, ok := md["referer"]; ok {
			referer = strings.Join(r, "")
		}
		if ua, ok := md["user-agent"]; ok {
			userAgent = strings.Join(ua, "")
		}
	}

	var b bytes.Buffer
	b.WriteString(addr)
	b.WriteString(" ")
	b.WriteString(logname)
	b.WriteString(" ")
	b.WriteString(username)
	b.WriteString(" ")
	b.WriteString(timestamp)
	b.WriteString(" ")
	b.WriteString(fmt.Sprintf("\"%s\"", request))
	b.WriteString(" ")
	b.WriteString(statusCode)
	b.WriteString(" ")
	b.WriteString(byteSize)
	b.WriteString(" ")
	b.WriteString(fmt.Sprintf("\"%s\"", referer))
	b.WriteString(" ")
	b.WriteString(fmt.Sprintf("\"%s\"", userAgent))

	log.Infof("%s", b.String())

	return
}

// identifyInterceptor call identifyUser with current context
func identifyInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	s, ok := info.Server.(*Server)
	if !ok {
		return nil, fmt.Errorf("unable to cast server")
	}
	accountID, err := identifyUser(ctx, s)
	if err != nil {
		return nil, err
	}

	ctx = context.WithValue(ctx, accountIDKey, accountID)

	return handler(ctx, req)
}

func startGRPCServer(address string, db db.PaperkidDB) error {
	// create a listener on TCP port
	lis, err := net.Listen("tcp", address)
	if err != nil {
		return fmt.Errorf("failed to listen: %v", err)
	}

	// create a server instance
	s := Server{
		paperdb: db,
	}

	// Create the gRPC server with the credentials and the interceptors
	opts := []grpc.ServerOption{
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				identifyInterceptor,
				logInterceptor,
			),
		),
	}

	// create a gRPC server object
	grpcServer := grpc.NewServer(opts...)

	// attach the Paperkid service to the server
	api.RegisterPaperkidServer(grpcServer, &s)

	// Register reflection service on gRPC server
	reflection.Register(grpcServer)

	// start the server
	log.Debugf("starting HTTP/2 gRPC server on %s", address)
	if err := grpcServer.Serve(lis); err != nil {
		return fmt.Errorf("failed to serve: %s", err)
	}

	return nil
}

func startRESTServer(address, grpcAddress string) error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux(runtime.WithIncomingHeaderMatcher(credentialMatcher))

	// Setup the client gRPC options
	opts := []grpc.DialOption{grpc.WithInsecure()}

	// Register paperkid
	err := api.RegisterPaperkidHandlerFromEndpoint(
		ctx,
		mux,
		grpcAddress,
		opts,
	)
	if err != nil {
		return fmt.Errorf("could not register service Paperkid: %s", err)
	}

	// Configure CORS options
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"http://localhost:8080", "http://localhost:7778", "http://localhost", "https://paperkid.news", "https://auth.paperkid.news", "https://api.paperkid.news"},
		AllowedMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
		AllowedHeaders: []string{"paperkid-token", "content-type"},
	})
	handler := c.Handler(mux)

	log.Debugf("starting HTTP/1.1 REST server on %s", address)
	http.ListenAndServe(address, handler)

	return nil
}

// Run start a gRPC server and waits for connection
func Run(conf Configuration, db db.PaperkidDB) {
	grpcAddress := fmt.Sprintf("%s:%d", conf.BindAddress, conf.BindPort)
	restAddress := fmt.Sprintf("%s:%d", conf.RestAddress, conf.RestPort)

	// fire the gRPC server in a goroutine
	go func() {
		err := startGRPCServer(grpcAddress, db)
		if err != nil {
			log.Fatalf("failed to start gRPC server: %s", err)
		}
	}()

	// fire the REST server in a goroutine
	go func() {
		err := startRESTServer(restAddress, grpcAddress)
		if err != nil {
			log.Fatalf("failed to start REST server: %s", err)
		}
	}()
}
