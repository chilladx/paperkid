package server

import (
	"path"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/gosimple/slug"

	"gitlab.com/chilladx/paperkid/api"
	"gitlab.com/chilladx/paperkid/db"
)

// Server represents the gRPC server
type Server struct {
	paperdb db.PaperkidDB
}

// ListFeeds list all the Feeds
func (s *Server) ListFeeds(ctx context.Context, req *api.ListFeedsRequest) (res *api.ListFeedsResponse, err error) {
	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := validatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	var uid *int64
	var isPublic bool
	if req.Restrict == api.Restrict_MINE {
		authID, err := getAuthIDFromContext(ctx)
		if err != nil {
			return nil, status.Errorf(codes.Unauthenticated, "Authentication has wrong format")
		}
		dbUser, err := s.paperdb.GetUserByAuthID(authID)
		if err != nil {
			return nil, status.Errorf(codes.Aborted, "Unable to get user: %s", err.Error())
		}

		uid = &dbUser.ID
	} else if req.Restrict == api.Restrict_PUBLIC {
		isPublic = true
	}

	dbFeeds, err := s.paperdb.ListFeeds(uid, isPublic, pageSize+1, pageOffset)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get feed list")
	}

	var nextPage bool
	if int32(len(dbFeeds)) > pageSize {
		dbFeeds = dbFeeds[:pageSize]
		nextPage = true
	}

	apiFeeds := make([]*api.Feed, 0)
	for _, feed := range dbFeeds {
		ts, err := ptypes.TimestampProto(feed.TsSubscribed)
		if err != nil {
			log.Warningf("Couldn't convert result timestamp %s to protobuf message. Skipping feed...", feed.TsSubscribed)
			continue
		}

		apiFeeds = append(apiFeeds, &api.Feed{
			Name:         path.Join(api.ResourceNameFeed, api.Base64Encode(feed.UUID)),
			UUID:         api.Base64Encode(feed.UUID),
			Slug:         feed.Slug,
			Title:        feed.Title,
			Description:  feed.Description,
			URL:          feed.URL,
			TsSubscribed: ts,
			IsPublic:     feed.IsPublic,
		})
	}

	res = &api.ListFeedsResponse{
		Feeds: apiFeeds,
	}

	if nextPage {
		res.NextPageToken, err = nextPageToken(req, pageOffset+uint64(len(apiFeeds)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return
}

// GetFeed returns a Feed
func (s *Server) GetFeed(ctx context.Context, req *api.GetFeedRequest) (*api.Feed, error) {
	reqUUID, err := api.ParseNameFeed(req.Name)
	if err != nil {
		log.Errorf("Can't parse feed name: %s", err)
		return nil, status.Errorf(codes.Aborted, "Unable to decode UUID (%s)", req.Name)
	}
	dbFeed, err := s.paperdb.GetFeed(reqUUID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get feed: %s", err.Error())
	}

	ts, err := ptypes.TimestampProto(dbFeed.TsSubscribed)
	if err != nil {
		log.Warningf("Couldn't convert result timestamp %s to protobuf message. Skipping feed...", dbFeed.TsSubscribed)
	}

	return &api.Feed{
		Name:         path.Join(api.ResourceNameFeed, api.Base64Encode(dbFeed.UUID)),
		Slug:         dbFeed.Slug,
		Title:        dbFeed.Title,
		Description:  dbFeed.Description,
		URL:          dbFeed.URL,
		TsSubscribed: ts,
		IsPublic:     dbFeed.IsPublic,
	}, nil
}

// CreateFeed creates a new Feed
func (s *Server) CreateFeed(ctx context.Context, req *api.CreateFeedRequest) (*api.Feed, error) {
	if req.Feed.Title == "" {
		return nil, status.Errorf(codes.InvalidArgument, "Missing title")
	}
	if req.Feed.URL == "" {
		return nil, status.Errorf(codes.InvalidArgument, "Missing URL")
	}

	dbFeed := db.Feed{
		Slug:     slug.Make(req.Feed.Title),
		Title:    req.Feed.Title,
		URL:      req.Feed.URL,
		IsPublic: req.Feed.IsPublic,
	}

	if err := dbFeed.PingFeed(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "URL doesn't responded valid RSS")
	}

	err := dbFeed.GrabDescription()
	if err != nil {
		return nil, status.Errorf(codes.Internal, "unable to get description: %s", err.Error())
	}

	createdFeed, err := s.paperdb.SaveFeed(dbFeed)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "%s", err.Error())
	}

	ts, err := ptypes.TimestampProto(createdFeed.TsSubscribed)
	if err != nil {
		log.Warningf("Couldn't convert result timestamp %s to protobuf message. Skipping feed...", createdFeed.TsSubscribed)
	}

	return &api.Feed{
		Name:         path.Join(api.ResourceNameFeed, api.Base64Encode(createdFeed.UUID)),
		UUID:         api.Base64Encode(createdFeed.UUID),
		Slug:         createdFeed.Slug,
		Title:        createdFeed.Title,
		Description:  createdFeed.Description,
		URL:          createdFeed.URL,
		TsSubscribed: ts,
		IsPublic:     createdFeed.IsPublic,
	}, nil
}

// UpdateFeed modify an existing Feed
func (s *Server) UpdateFeed(ctx context.Context, req *api.UpdateFeedRequest) (*api.Feed, error) {
	if authorized, err := checkUserRole(ctx, s.paperdb, api.User_ADMIN); !authorized || err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Unauthorized call")
	}

	reqUUID, err := api.ParseNameFeed(req.Feed.Name)
	if err != nil {
		log.Errorf("Can't parse feed name: %s", err)
		return nil, status.Errorf(codes.Aborted, "Unable to decode UUID (%s)", req.Feed.Name)
	}
	dbFeed, err := s.paperdb.GetFeed(reqUUID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get feed: %s", err.Error())
	}

	if req.UpdateMask == nil || Contains(req.UpdateMask.Paths, "title") {
		dbFeed.Title = req.Feed.Title
	}
	if req.UpdateMask == nil || Contains(req.UpdateMask.Paths, "URL") {
		dbFeed.URL = req.Feed.URL
	}
	if req.UpdateMask == nil || Contains(req.UpdateMask.Paths, "IsPublic") {
		dbFeed.IsPublic = req.Feed.IsPublic
	}
	dbFeed.Slug = slug.Make(dbFeed.Title)

	if req.UpdateMask == nil || Contains(req.UpdateMask.Paths, "description") {
		dbFeed.Description = req.Feed.Description
	}

	updatedFeed, err := s.paperdb.SaveFeed(*dbFeed)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "unable to save the feed: %s", err.Error())
	}

	ts, err := ptypes.TimestampProto(updatedFeed.TsSubscribed)
	if err != nil {
		log.Warningf("Couldn't convert result timestamp %s to protobuf message. Skipping feed...", updatedFeed.TsSubscribed)
	}

	return &api.Feed{
		Name:         path.Join(api.ResourceNameFeed, api.Base64Encode(updatedFeed.UUID)),
		UUID:         api.Base64Encode(updatedFeed.UUID),
		Slug:         updatedFeed.Slug,
		Title:        updatedFeed.Title,
		Description:  updatedFeed.Description,
		URL:          updatedFeed.URL,
		TsSubscribed: ts,
		IsPublic:     updatedFeed.IsPublic,
	}, nil
}

// SubscribeFeed creates a link between an existing Feed and an existing User
func (s *Server) SubscribeFeed(ctx context.Context, req *api.SubscribeFeedRequest) (*api.Feed, error) {
	reqUUID, err := api.ParseNameFeed(req.Feed.Name)
	if err != nil {
		log.Errorf("Can't parse feed name: %s", err)
		return nil, status.Errorf(codes.Aborted, "Unable to decode UUID (%s)", req.Feed.Name)
	}
	dbFeed, err := s.paperdb.GetFeed(reqUUID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get feed: %s", err.Error())
	}

	authID, err := getAuthIDFromContext(ctx)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Authentication has wrong format")
	}
	dbUser, err := s.paperdb.GetUserByAuthID(authID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get user: %s", err.Error())
	}

	updatedFeed, err := s.paperdb.SubscribeFeed(*dbFeed, &dbUser.ID)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "unable to subscribe to the feed: %s", err.Error())
	}

	ts, err := ptypes.TimestampProto(updatedFeed.TsSubscribed)
	if err != nil {
		log.Warningf("Couldn't convert result timestamp %s to protobuf message. Skipping feed...", updatedFeed.TsSubscribed)
	}

	return &api.Feed{
		Name:         path.Join(api.ResourceNameFeed, api.Base64Encode(updatedFeed.UUID)),
		UUID:         api.Base64Encode(updatedFeed.UUID),
		Slug:         updatedFeed.Slug,
		Title:        updatedFeed.Title,
		Description:  updatedFeed.Description,
		URL:          updatedFeed.URL,
		TsSubscribed: ts,
		IsPublic:     updatedFeed.IsPublic,
	}, nil
}

// UnsubscribeFeed deletes the link between a Feed and a User
func (s *Server) UnsubscribeFeed(ctx context.Context, req *api.UnsubscribeFeedRequest) (*api.Feed, error) {
	reqUUID, err := api.ParseNameFeed(req.Feed.Name)
	if err != nil {
		log.Errorf("Can't parse feed name: %s", err)
		return nil, status.Errorf(codes.Aborted, "Unable to decode UUID (%s)", req.Feed.Name)
	}
	dbFeed, err := s.paperdb.GetFeed(reqUUID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get feed: %s", err.Error())
	}

	authID, err := getAuthIDFromContext(ctx)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Authentication has wrong format")
	}
	dbUser, err := s.paperdb.GetUserByAuthID(authID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get user: %s", err.Error())
	}

	updatedFeed, err := s.paperdb.UnsubscribeFeed(*dbFeed, &dbUser.ID)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "unable to unsubscribe to the feed: %s", err.Error())
	}

	ts, err := ptypes.TimestampProto(updatedFeed.TsSubscribed)
	if err != nil {
		log.Warningf("Couldn't convert result timestamp %s to protobuf message. Skipping feed...", updatedFeed.TsSubscribed)
	}

	return &api.Feed{
		Name:         path.Join(api.ResourceNameFeed, api.Base64Encode(updatedFeed.UUID)),
		UUID:         api.Base64Encode(updatedFeed.UUID),
		Slug:         updatedFeed.Slug,
		Title:        updatedFeed.Title,
		Description:  updatedFeed.Description,
		URL:          updatedFeed.URL,
		TsSubscribed: ts,
		IsPublic:     updatedFeed.IsPublic,
	}, nil
}

// DeleteFeed delete an existing Feed
func (s *Server) DeleteFeed(ctx context.Context, req *api.DeleteFeedRequest) (*empty.Empty, error) {
	reqUUID, err := api.ParseNameFeed(req.Name)
	if err != nil {
		log.Errorf("Can't parse feed name: %s", err)
		return nil, status.Errorf(codes.Aborted, "Unable to decode UUID (%s)", req.Name)
	}
	dbFeed, err := s.paperdb.GetFeed(reqUUID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get feed %s: %s", reqUUID.String(), err.Error())
	}

	err = s.paperdb.DeleteFeed(*dbFeed)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to delete feed: %s", err.Error())
	}

	return &empty.Empty{}, nil
}

// ListArticles list all the Articles for a Feed
func (s *Server) ListArticles(ctx context.Context, req *api.ListArticlesRequest) (res *api.ListArticlesResponse, err error) {
	reqFeedUUID, err := api.ParseNameFeed(req.Parent)
	if err != nil {
		log.Errorf("Can't parse feed name: %s", err)
		return nil, status.Errorf(codes.Aborted, "Unable to decode UUID (%s)", req.Parent)
	}

	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := validatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	authID, err := getAuthIDFromContext(ctx)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Authentication has wrong format")
	}
	var uid int64
	if authID > 0 {
		dbUser, err := s.paperdb.GetUserByAuthID(authID)
		if err != nil {
			return nil, status.Errorf(codes.Aborted, "Unable to get user: %s", err.Error())
		}
		uid = dbUser.ID
	}

	dbArticles, err := s.paperdb.ListArticles([]uuid.UUID{reqFeedUUID}, uid, req.UnreadOnly, req.IsPublic, pageSize+1, pageOffset)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get articles list: %s", err.Error())
	}

	var nextPage bool
	if int32(len(dbArticles)) > pageSize {
		dbArticles = dbArticles[:pageSize]
		nextPage = true
	}

	apiArticles := make([]*api.Article, 0)
	for _, article := range dbArticles {
		apiArticles = append(apiArticles, &api.Article{
			Name:  path.Join(api.ResourceNameFeed, api.Base64Encode(reqFeedUUID), api.ResourceNameArticle, api.Base64Encode(article.UUID)),
			UUID:  api.Base64Encode(article.UUID),
			Slug:  article.Slug,
			Title: article.Title,
			URL:   article.URL,
			Body:  article.Body,
			Read:  article.Read,
		})
	}

	res = &api.ListArticlesResponse{
		Articles: apiArticles,
	}

	if nextPage {
		res.NextPageToken, err = nextPageToken(req, pageOffset+uint64(len(apiArticles)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return
}

// ListAllArticles list all the Articles
func (s *Server) ListAllArticles(ctx context.Context, req *api.ListArticlesRequest) (res *api.ListArticlesResponse, err error) {
	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := validatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	authID, err := getAuthIDFromContext(ctx)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Authentication has wrong format")
	}
	var uid int64
	if authID > 0 {
		dbUser, err := s.paperdb.GetUserByAuthID(authID)
		if err != nil {
			return nil, status.Errorf(codes.Aborted, "Unable to get user: %s", err.Error())
		}
		uid = dbUser.ID
	}

	dbArticles, err := s.paperdb.ListArticles([]uuid.UUID{}, uid, req.UnreadOnly, req.IsPublic, pageSize+1, pageOffset)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get articles list: %s", err.Error())
	}

	var nextPage bool
	if int32(len(dbArticles)) > pageSize {
		dbArticles = dbArticles[:pageSize]
		nextPage = true
	}

	apiArticles := make([]*api.Article, 0)
	for _, article := range dbArticles {
		apiArticles = append(apiArticles, &api.Article{
			Name:  path.Join(api.ResourceNameFeed, api.Base64Encode(article.FeedUUID), api.ResourceNameArticle, api.Base64Encode(article.UUID)),
			UUID:  api.Base64Encode(article.UUID),
			Slug:  article.Slug,
			Title: article.Title,
			URL:   article.URL,
			Body:  article.Body,
			Read:  article.Read,
		})
	}

	res = &api.ListArticlesResponse{
		Articles: apiArticles,
	}

	if nextPage {
		res.NextPageToken, err = nextPageToken(req, pageOffset+uint64(len(apiArticles)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return
}

// QueryArticles list the Articles matching criteria
func (s *Server) QueryArticles(ctx context.Context, req *api.QueryArticlesRequest) (res *api.QueryArticlesResponse, err error) {
	reqFeedUUIDs := make([]uuid.UUID, len(req.Feeds))
	for i, reqFeed := range req.Feeds {
		reqFeedUUIDs[i], err = api.ParseNameFeed(reqFeed)
		if err != nil {
			log.Errorf("Can't parse feed name: %s", err)
			continue
		}
	}

	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := validatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	authID, err := getAuthIDFromContext(ctx)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Authentication has wrong format")
	}
	var uid int64
	if authID > 0 {
		dbUser, err := s.paperdb.GetUserByAuthID(authID)
		if err != nil {
			return nil, status.Errorf(codes.Aborted, "Unable to get user: %s", err.Error())
		}
		uid = dbUser.ID
	}

	dbArticles, err := s.paperdb.ListArticles(reqFeedUUIDs, uid, req.UnreadOnly, req.IsPublic, pageSize+1, pageOffset)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get articles list: %s", err.Error())
	}

	var nextPage bool
	if int32(len(dbArticles)) > pageSize {
		dbArticles = dbArticles[:pageSize]
		nextPage = true
	}

	apiArticles := make([]*api.Article, 0)
	for _, article := range dbArticles {
		apiArticles = append(apiArticles, &api.Article{
			Name:  path.Join(api.ResourceNameFeed, api.Base64Encode(article.FeedUUID), api.ResourceNameArticle, api.Base64Encode(article.UUID)),
			UUID:  api.Base64Encode(article.UUID),
			Slug:  article.Slug,
			Title: article.Title,
			URL:   article.URL,
			Body:  article.Body,
			Read:  article.Read,
		})
	}

	res = &api.QueryArticlesResponse{
		Articles: apiArticles,
	}

	if nextPage {
		res.NextPageToken, err = nextPageToken(req, pageOffset+uint64(len(apiArticles)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return
}

// GetArticle returns an Article
func (s *Server) GetArticle(ctx context.Context, req *api.GetArticleRequest) (*api.Article, error) {
	reqFeedUUID, reqArticleUUID, err := api.ParseNameArticle(req.Name)
	if err != nil {
		log.Errorf("Can't parse feed name: %s", err)
		return nil, status.Errorf(codes.Aborted, "Unable to decode UUID (%s)", req.Name)
	}

	dbArticle, err := s.paperdb.GetArticle(reqFeedUUID, reqArticleUUID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get article: %s", err.Error())
	}

	return &api.Article{
		Name:  path.Join(api.ResourceNameFeed, api.Base64Encode(reqFeedUUID), api.ResourceNameArticle, api.Base64Encode(dbArticle.UUID)),
		UUID:  api.Base64Encode(dbArticle.UUID),
		Slug:  dbArticle.Slug,
		Title: dbArticle.Title,
		URL:   dbArticle.URL,
		Body:  dbArticle.Body,
		Read:  dbArticle.Read,
	}, nil
}

// UpdateArticle returns an Article
func (s *Server) UpdateArticle(ctx context.Context, req *api.UpdateArticleRequest) (*api.Article, error) {
	reqFeedUUID, reqArticleUUID, err := api.ParseNameArticle(req.Article.Name)
	if err != nil {
		log.Errorf("Can't parse feed name: %s", err)
		return nil, status.Errorf(codes.Aborted, "Unable to decode UUID (%s)", req.Article.Name)
	}

	authID, err := getAuthIDFromContext(ctx)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Authentication has wrong format")
	}
	dbUser, err := s.paperdb.GetUserByAuthID(authID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get user: %s", err.Error())
	}

	dbArticle, err := s.paperdb.GetArticle(reqFeedUUID, reqArticleUUID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get article: %s", err.Error())
	}

	if dbArticle.Slug != req.Article.Slug {
		dbArticle.Slug = req.Article.Slug
	}
	if dbArticle.Title != req.Article.Title {
		dbArticle.Title = req.Article.Title
	}
	if dbArticle.URL != req.Article.URL {
		dbArticle.URL = req.Article.URL
	}
	if dbArticle.Body != req.Article.Body {
		dbArticle.Body = req.Article.Body
	}
	if dbArticle.Read != req.Article.Read {
		dbArticle.Read = req.Article.Read
	}

	updatedArticle, err := s.paperdb.SaveArticle(*dbArticle, &dbUser.ID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to update article: %s", err.Error())
	}

	return &api.Article{
		Name:  path.Join(api.ResourceNameFeed, api.Base64Encode(updatedArticle.FeedUUID), api.ResourceNameArticle, api.Base64Encode(updatedArticle.UUID)),
		UUID:  api.Base64Encode(updatedArticle.UUID),
		Slug:  updatedArticle.Slug,
		Title: updatedArticle.Title,
		URL:   updatedArticle.URL,
		Body:  updatedArticle.Body,
		Read:  updatedArticle.Read,
	}, nil
}

// ReadArticle updates an Article on the Read property
func (s *Server) ReadArticle(ctx context.Context, req *api.ReadArticleRequest) (*api.Article, error) {
	if authorized, err := checkUserRole(ctx, s.paperdb, api.User_USER); !authorized || err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Unauthorized call")
	}

	reqFeedUUID, reqArticleUUID, err := api.ParseNameArticle(req.Name)
	if err != nil {
		log.Errorf("Can't parse feed name: %s", err)
		return nil, status.Errorf(codes.Aborted, "Unable to decode UUID (%s)", req.Name)
	}

	dbArticle, err := s.paperdb.GetArticle(reqFeedUUID, reqArticleUUID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get article: %s", err.Error())
	}

	dbArticle.Read = true

	authID, err := getAuthIDFromContext(ctx)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Authentication has wrong format")
	}
	dbUser, err := s.paperdb.GetUserByAuthID(authID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get user: %s", err.Error())
	}

	updatedArticle, err := s.paperdb.SaveArticle(*dbArticle, &dbUser.ID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to update article: %s", err.Error())
	}

	return &api.Article{
		Name:  path.Join(api.ResourceNameFeed, api.Base64Encode(reqFeedUUID), api.ResourceNameArticle, api.Base64Encode(updatedArticle.UUID)),
		UUID:  api.Base64Encode(updatedArticle.UUID),
		Slug:  updatedArticle.Slug,
		Title: updatedArticle.Title,
		URL:   updatedArticle.URL,
		Body:  updatedArticle.Body,
		Read:  updatedArticle.Read,
	}, nil
}

// CrawlArticles crawls the Articles for a Feed
func (s *Server) CrawlArticles(ctx context.Context, req *api.CrawlArticlesRequest) (*api.CrawlArticlesResponse, error) {
	reqFeedUUID, err := api.ParseNameFeed(req.Parent)
	if err != nil {
		log.Errorf("Can't parse feed name: %s", err)
		return nil, status.Errorf(codes.Aborted, "Unable to decode UUID (%s)", req.Parent)
	}

	dbFeed, err := s.paperdb.GetFeed(reqFeedUUID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get feed: %s", err.Error())
	}

	dbArticles, err := s.paperdb.CrawlArticles(dbFeed)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to crawl Feed: %s", err.Error())
	}

	apiArticles := make([]*api.Article, 0)
	for _, article := range dbArticles {
		apiArticles = append(apiArticles, &api.Article{
			Name:  path.Join(api.ResourceNameFeed, api.Base64Encode(reqFeedUUID), api.ResourceNameArticle, api.Base64Encode(article.UUID)),
			UUID:  api.Base64Encode(article.UUID),
			Slug:  article.Slug,
			Title: article.Title,
			URL:   article.URL,
			Body:  article.Body,
			Read:  article.Read,
		})
	}

	return &api.CrawlArticlesResponse{Articles: apiArticles}, nil
}

// ListUsers list all the Users
// CAUTION: Restricted to api.User_ADMIN role
func (s *Server) ListUsers(ctx context.Context, req *api.ListUsersRequest) (res *api.ListUsersResponse, err error) {
	if authorized, err := checkUserRole(ctx, s.paperdb, api.User_ADMIN); !authorized || err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Unauthorized call")
	}

	var pageOffset uint64
	if req.PageToken != "" {
		valid, err := validatePageToken(req.PageToken, req, &pageOffset)
		if err != nil {
			log.Errorf("Failed to validate page token: %s", err)
			return nil, status.Errorf(codes.InvalidArgument, "Invalid page token")
		}
		if !valid {
			log.Errorf("Page token does not match request")
			return nil, status.Errorf(codes.InvalidArgument, "Page token does not match request")
		}
	}

	pageSize := req.PageSize
	if pageSize == 0 {
		pageSize = defaultPageSize
	} else if pageSize > maxPageSize {
		pageSize = maxPageSize
	}

	dbUsers, err := s.paperdb.ListUsers(pageSize+1, pageOffset)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get user list")
	}

	var nextPage bool
	if int32(len(dbUsers)) > pageSize {
		dbUsers = dbUsers[:pageSize]
		nextPage = true
	}

	apiUsers := make([]*api.User, 0)
	for _, user := range dbUsers {
		roles := make([]api.User_Role, len(user.Role))
		for i, r := range user.Role {
			roles[i] = api.User_Role(r)
		}

		ts, err := ptypes.TimestampProto(user.TsLocked)
		if err != nil {
			log.Warningf("Couldn't convert lock timestamp %s to protobuf message. Skipping user...", user.TsLocked)
			continue
		}

		apiUsers = append(apiUsers, &api.User{
			Name:          path.Join(api.ResourceNameUser, api.Base64Encode(user.UUID)),
			UUID:          api.Base64Encode(user.UUID),
			Email:         user.Email,
			Configuration: string(user.Configuration),
			Role:          roles,
			TsLock:        ts,
		})
	}

	res = &api.ListUsersResponse{
		Users: apiUsers,
	}

	if nextPage {
		res.NextPageToken, err = nextPageToken(req, pageOffset+uint64(len(apiUsers)))
		if err != nil {
			log.Warning("Failed to generate next page token")
			// Return values but don't set page token
			res.NextPageToken = ""
		}
	}

	return
}

// GetUser returns a User
func (s *Server) GetUser(ctx context.Context, req *api.GetUserRequest) (*api.User, error) {
	reqUUID, err := api.ParseNameUser(req.Name)
	if err != nil {
		log.Errorf("Can't parse user name: %s", err)
		return nil, status.Errorf(codes.Aborted, "Unable to decode UUID (%s)", req.Name)
	}
	dbUser, err := s.paperdb.GetUser(reqUUID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get user: %s", err.Error())
	}

	roles := make([]api.User_Role, len(dbUser.Role))
	for i, r := range dbUser.Role {
		roles[i] = api.User_Role(r)
	}

	ts, err := ptypes.TimestampProto(dbUser.TsLocked)
	if err != nil {
		log.Warningf("Couldn't convert lock timestamp %s to protobuf message.", dbUser.TsLocked)
	}

	return &api.User{
		Name:          path.Join(api.ResourceNameUser, api.Base64Encode(dbUser.UUID)),
		UUID:          api.Base64Encode(dbUser.UUID),
		Email:         dbUser.Email,
		Configuration: string(dbUser.Configuration),
		Role:          roles,
		TsLock:        ts,
	}, nil
}

// GetSelfUser returns the User linked to AuthID
func (s *Server) GetSelfUser(ctx context.Context, req *api.GetUserSelfRequest) (*api.User, error) {
	if req.Email == "" {
		return nil, status.Errorf(codes.InvalidArgument, "Missing email")
	}
	authID, err := getAuthIDFromContext(ctx)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Authentication has wrong format")
	}

	dbUser, err := s.paperdb.GetUserByAuthIDAndEmail(authID, req.Email)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get user: %s", err.Error())
	}

	if !dbUser.TsLocked.IsZero() {
		return nil, status.Errorf(codes.PermissionDenied, "User is locked")
	}

	roles := make([]api.User_Role, len(dbUser.Role))
	for i, r := range dbUser.Role {
		roles[i] = api.User_Role(r)
	}

	ts, err := ptypes.TimestampProto(dbUser.TsLocked)
	if err != nil {
		log.Warningf("Couldn't convert lock timestamp %s to protobuf message.", dbUser.TsLocked)
	}

	return &api.User{
		Name:          path.Join(api.ResourceNameUser, api.Base64Encode(dbUser.UUID)),
		UUID:          api.Base64Encode(dbUser.UUID),
		Email:         dbUser.Email,
		Configuration: string(dbUser.Configuration),
		Role:          roles,
		TsLock:        ts,
	}, nil
}

// CreateUser creates a new User
func (s *Server) CreateUser(ctx context.Context, req *api.CreateUserRequest) (*api.User, error) {
	if req.User.Email == "" {
		return nil, status.Errorf(codes.InvalidArgument, "Missing email")
	}

	authID, err := getAuthIDFromContext(ctx)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Authentication has wrong format")
	}

	if req.User.Email == "" {
		return nil, status.Errorf(codes.InvalidArgument, "Missing email")
	}

	if req.User.Configuration == "" {
		return nil, status.Errorf(codes.InvalidArgument, "Missing configuration")
	}

	dbUser := db.User{
		AuthID:        authID,
		Email:         req.User.Email,
		Configuration: []byte(req.User.Configuration),
	}

	createdUser, err := s.paperdb.SaveUser(dbUser)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "%s", err.Error())
	}

	roles := make([]api.User_Role, len(createdUser.Role))
	for i, r := range createdUser.Role {
		roles[i] = api.User_Role(r)
	}

	ts, err := ptypes.TimestampProto(createdUser.TsLocked)
	if err != nil {
		log.Warningf("Couldn't convert lock timestamp %s to protobuf message.", createdUser.TsLocked)
	}

	return &api.User{
		Name:          path.Join(api.ResourceNameUser, api.Base64Encode(createdUser.UUID)),
		UUID:          api.Base64Encode(createdUser.UUID),
		Email:         createdUser.Email,
		Configuration: string(createdUser.Configuration),
		Role:          roles,
		TsLock:        ts,
	}, nil
}

// UpdateUser modify an existing User
func (s *Server) UpdateUser(ctx context.Context, req *api.UpdateUserRequest) (*api.User, error) {
	reqUUID, err := api.ParseNameUser(req.User.Name)
	if err != nil {
		log.Errorf("Can't parse user name: %s", err)
		return nil, status.Errorf(codes.Aborted, "Unable to decode UUID (%s)", req.User.Name)
	}
	dbUser, err := s.paperdb.GetUser(reqUUID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get user: %s", err.Error())
	}

	authID, err := getAuthIDFromContext(ctx)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Authentication has wrong format")
	}
	if authID != dbUser.AuthID {
		if authorized, err := checkUserRole(ctx, s.paperdb, api.User_ADMIN); !authorized || err != nil {
			return nil, status.Errorf(codes.Unauthenticated, "Unauthorized call")
		}
	}

	if req.UpdateMask == nil || Contains(req.UpdateMask.Paths, "configuration") {
		dbUser.Configuration = []byte(req.User.Configuration)
	}

	if req.UpdateMask == nil || Contains(req.UpdateMask.Paths, "role") {
		dbUser.Role = make([]db.UserRole, len(req.User.Role))
		for i, role := range req.User.Role {
			dbUser.Role[i] = db.UserRole(role)
		}
	}

	if req.UpdateMask == nil || Contains(req.UpdateMask.Paths, "ts_lock") {
		dbUser.TsLocked, err = ptypes.Timestamp(req.User.TsLock)
		if err != nil {
			log.Errorf("invalid timestamp (%s)", err)
			return nil, status.Error(codes.InvalidArgument, "invalid timestamp")
		}
	}

	updatedUser, err := s.paperdb.SaveUser(*dbUser)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "unable to save the user: %s", err.Error())
	}

	roles := make([]api.User_Role, len(updatedUser.Role))
	for i, r := range updatedUser.Role {
		roles[i] = api.User_Role(r)
	}

	ts, err := ptypes.TimestampProto(updatedUser.TsLocked)
	if err != nil {
		log.Warningf("Couldn't convert lock timestamp %s to protobuf message.", updatedUser.TsLocked)
	}

	return &api.User{
		Name:          path.Join(api.ResourceNameUser, api.Base64Encode(updatedUser.UUID)),
		UUID:          api.Base64Encode(updatedUser.UUID),
		Email:         updatedUser.Email,
		Configuration: string(updatedUser.Configuration),
		Role:          roles,
		TsLock:        ts,
	}, nil
}

// DeleteUser delete an existing User
func (s *Server) DeleteUser(ctx context.Context, req *api.DeleteUserRequest) (*empty.Empty, error) {
	reqUUID, err := api.ParseNameUser(req.Name)
	if err != nil {
		log.Errorf("Can't parse user name: %s", err)
		return nil, status.Errorf(codes.Aborted, "Unable to decode UUID (%s)", req.Name)
	}
	dbUser, err := s.paperdb.GetUser(reqUUID)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to get user %s: %s", reqUUID.String(), err.Error())
	}

	authID, err := getAuthIDFromContext(ctx)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "Authentication has wrong format")
	}
	if authID != dbUser.AuthID {
		if authorized, err := checkUserRole(ctx, s.paperdb, api.User_ADMIN); !authorized || err != nil {
			return nil, status.Errorf(codes.Unauthenticated, "Unauthorized call")
		}
	}

	err = s.paperdb.DeleteUser(*dbUser)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "Unable to delete user: %s", err.Error())
	}

	return &empty.Empty{}, nil
}
