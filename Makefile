PKG = "gitlab.com/chilladx/paperkid"
PKG_BUILD := "${PKG}/cmd/paperkid"
PKG_CLIENT_BUILD := "${PKG}/cmd/paperkid-cli"
GITHASH = $(shell git describe --always --long --dirty)
GO_FILES := $(shell find . -name '*.go' | grep -v '/vendor/')
GO_PACKAGES := $(shell go list ./... | grep -v /vendor/)
BINDIR := "bin"
OUT := "${BINDIR}/paperkid"
OUT_CLIENT := "${BINDIR}/paperkid-cli"

PHONY_COMMON = dep fmt lint test race msan help

dep: ## Fetch dependencies
	@go get -v -d ./...
	@go get gopkg.in/DATA-DOG/go-sqlmock.v1

dep-web: ## Fetch dependencies for the web-ui
	@$(MAKE) -C www dep

api: paperkid.pb.go paperkid.pb.gw.go paperkid.swagger.json ## Auto-generate grpc/REST Gateway Go sources

paperkid.pb.go: api/paperkid.proto
	@protoc -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--go_out=plugins=grpc:. \
		api/paperkid.proto

paperkid.pb.gw.go: api/paperkid.proto
	@protoc -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		api/paperkid.proto

paperkid.swagger.json: api/paperkid.proto
	@protoc -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--swagger_out=logtostderr=true:. \
		api/paperkid.proto

build: api build_server build_web ## Build the project (api, server and cli)

build_server: api ## Build the server file
	@go build -i -v -o $(OUT) -ldflags="-X main.githash=${GITHASH}" $(PKG_BUILD)

build_web: dep-web ## Build the web application
	@$(MAKE) -C www build

clean: ## Remove previous builds
	@rm -f $(OUT)

fmt: ## Format Go files
	@gofmt -w ${GO_FILES}

files: ## List all Go files
	@echo ${GO_FILES}

lint: ## Run the linter on all Go files
	@golint -set_exit_status ${GO_PACKAGES}

test: dep ## Run tests
	@go test -short ${GO_PACKAGES}

race: dep ## Run tests with race detector
	@go test -race -short ${GO_PACKAGES}

msan: dep ## Run tests with memory sanitizer
	@go test -msan -short ${GO_PACKAGES}

coverage: ## Generate code coverage
	@tools/coverage.sh

coveragehtml: ## Generate code coverage report
	@tools/coverage.sh html

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
