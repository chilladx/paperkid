# paperkid

Paperkid is an RSS feed aggregator. The crawler and API are written in go and web-ui is a VueJS application.

## Installing / Getting started

To start the application, get the [docker-compose file](docker/docker-compose.yml) and fire it up:

```sh
$ docker-compose -d -f docker/docker-compose.yml up
```

Now go to http://localhost:8080/, enjoy!

Here are the environment variables you can use to configure the services:

| Service     | Variable                    | Description                                                                                        | Default                             |
|-------------|-----------------------------|----------------------------------------------------------------------------------------------------|-------------------------------------|
| paperkid    | PAPERKID_VERBOSE            | set verbosity level (0: silent, 1: warnings and errors, 2: verbose, 3: debug)                      | 1                                   |
| paperkid    | PAPERKID_BINDADDRESS        | Address to bind gRPC API to                                                                        | 0.0.0.0                             |
| paperkid    | PAPERKID_PORT               | Port number to bind gRPC API to                                                                    | 7777                                |
| paperkid    | PAPERKID_RESTADDRESS        | Address to bind REST API to                                                                        | 0.0.0.0                             |
| paperkid    | PAPERKID_RESTPORT           | Port number to bind REST API to                                                                    | 7778                                |
| paperkid    | PAPERKID_AUTHISSUER         | Public address of the Auth service                                                                 | http://localhost:8765               |
| paperkid    | PAPERKID_AUTHPRIVATEADDRESS | Private address of the Auth service                                                                | http://authn:3000                   |
| paperkid    | PAPERKID_AUTHDOMAIN         | Public domain of the Paperkid service                                                              | localhost                           |
| paperkid    | PAPERKID_AUTHUSERNAME       | Private login to the Auth service                                                                  | login                               |
| paperkid    | PAPERKID_AUTHPASSWORD       | Private password to the Auth service                                                               | password                            |
| paperkid    | PAPERKID_DBPATH             | Path to a SQLite database file                                                                     | /var/lib/paperkid/paperkid.sqlite   |
| paperkid    | PAPERKID_FREQUENCY          | Crawl frequency (in minutes)                                                                       | 60                                  |
| paperkidweb | PAPERKID_AUTH               | Public address of the Auth service                                                                 | http://localhost:8765               |
| paperkidweb | PAPERKID_API                | Pubic address of the REST API service                                                              | http://localhost:7778/v1            |
| authn       | DATABASE_URL                | The database URL specifies the driver, host, port, database name, and connection credentials       | mysql://root:password@db:3306/authn |
| authn       | REDIS_URL                   | Redis is the preferred database for session refresh tokens, encrypted blobs, and active user stats | redis://redis:6379/0                |
| authn       | AUTHN_URL                   | Public address of the Auth service                                                                 | http://localhost:8765               |
| authn       | APP_DOMAINS                 | Public domain of the Paperkid service                                                              | localhost                           |
| authn       | HTTP_AUTH_USERNAME          | Private login to the Auth service                                                                  | login                               |
| authn       | HTTP_AUTH_PASSWORD          | Private password to the Auth service                                                               | password                            |
| authn       | SECRET_KEY_BASE             | Any HMAC keys used by AuthN will be derived from this base value                                   | xxx                                 |
| authn       | USERNAME_IS_EMAIL           | If you ask users to sign up with an email address, enable this so that AuthN can validate properly | true                                |
| db          | MYSQL_ROOT_PASSWORD         | Password for root account in MySQL                                                                 | password                            |
| db          | MYSQL_DATABASE              | Name of the Authn database                                                                         | authn                               |
| db          | MYSQL_ALLOW_EMPTY_PASSWORD  |  Set it to true to allow the container to be started with a blank password for the root user       | yes                                 |

### Initial Configuration

In order to init the authn server, on the very first run, and before creating any user in the application, you need to run the following:

```sh
$ docker exec -it authn sh -c "./authn migrate"
```

## Developing

Here's a brief intro about what a developer must do in order to start developing the project further:

```sh
$ git clone git@gitlab.com:chilladx/paperkid.git
$ cd paperkid
$ make dep
$ make dep_web
```

### Building

To build the applications, you need to run

```sh
$ make build
```

### Run the web development environment

```sh
$ cd www/
www/ $ npm run webui
```

## License

Paperkid is released under the [MIT License](LICENSE).