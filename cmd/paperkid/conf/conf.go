package conf

import (
	"gitlab.com/pantomath-io/panto/util"
)

// Options is a list of the panto executable
var Options = []util.Option{
	// Command-only options
	{
		Default: false,
		Usage:   "suppress all output",
		Flag:    "quiet",
		Short:   "q",
	},
	{
		Default: false,
		Usage:   "display version and exit",
		Flag:    "version",
		Short:   "V",
	},
	{
		Default: "",
		Usage:   "path to configuration file",
		Flag:    "conf",
	},
	// General options
	{
		Name:    "verbose",
		Default: 1,
		Usage:   "set verbosity level (0: silent, 1: warnings and errors, 2: verbose, 3: debug)",
		Flag:    "verbose",
		Short:   "v",
		Env:     "PAPERKID_VERBOSE",
	},
	// gRPC server options
	{
		Name:    "server.bindaddress",
		Default: "0.0.0.0",
		Usage:   "Address to bind gRPC API to",
		Flag:    "bind-address",
		Env:     "PAPERKID_BINDADDRESS",
	},
	{
		Name:    "server.port",
		Default: "7777",
		Usage:   "Port number to bind gRPC API to",
		Flag:    "port",
		Env:     "PAPERKID_PORT",
	},
	// REST server options
	{
		Name:    "server.restaddress",
		Default: "0.0.0.0",
		Usage:   "Address to bind REST API to",
		Flag:    "rest-address",
		Env:     "PAPERKID_RESTADDRESS",
	},
	{
		Name:    "server.restport",
		Default: "7778",
		Usage:   "Port number to bind REST API to",
		Flag:    "rest-port",
		Env:     "PAPERKID_RESTPORT",
	},
	// Auth server options
	{
		Name:    "auth.issuer",
		Default: "http://localhost:8765",
		Usage:   "Public address of the Auth service",
		Env:     "PAPERKID_AUTHISSUER",
	},
	{
		Name:    "auth.privateaddress",
		Default: "http://authn:3000",
		Usage:   "Private address of the Auth service",
		Env:     "PAPERKID_AUTHPRIVATEADDRESS",
	},
	{
		Name:    "auth.domain",
		Default: "localhost",
		Usage:   "Public domain of the Paperkid service",
		Env:     "PAPERKID_AUTHDOMAIN",
	},
	{
		Name:    "auth.username",
		Default: "username",
		Usage:   "Private login to the Auth service",
		Env:     "PAPERKID_AUTHUSERNAME",
	},
	{
		Name:    "auth.password",
		Default: "password",
		Usage:   "Private password to the Auth service",
		Env:     "PAPERKID_AUTHPASSWORD",
	},
	// SQLite options
	{
		Name:    "db.path",
		Default: "/var/lib/paperkid/paperkid.sqlite",
		Usage:   "Path to a SQLite database file",
		Flag:    "dbconf-addr",
		Env:     "PAPERKID_DBPATH",
	},
	// Crawl options
	{
		Name:    "crawl.frequency",
		Default: 60,
		Usage:   "Crawl frequency (in minutes)",
		Flag:    "frequency",
		Env:     "PAPERKID_FREQUENCY",
	},
}
