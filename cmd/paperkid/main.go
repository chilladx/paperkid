package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/op/go-logging"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/chilladx/paperkid/cmd/paperkid/conf"
	"gitlab.com/chilladx/paperkid/db"
	"gitlab.com/chilladx/paperkid/server"
	"gitlab.com/pantomath-io/panto/util"
)

// Create a new instance of the logger
var log = logging.MustGetLogger("main")

// program ID
var (
	program = "paperkid"
	version = "0.0.0"
	githash = ""
)

// configureLogger chooses the backend(s) to set
func configureLogger() {
	// Configure log level, select most verbose first
	verbose := viper.GetInt("verbose")
	if verbose > 3 {
		verbose = 3
	}
	var logLevel logging.Level
	switch verbose {
	case 0:
		// silence output, don't configure any loggers
		logging.SetBackend()
		return
	case 1:
		logLevel = logging.WARNING
	case 2:
		logLevel = logging.INFO
	case 3:
		logLevel = logging.DEBUG
	default:
		logLevel = logging.WARNING
	}

	var backendList = make([]logging.Backend, 0, 3)

	var format = logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} [%{level:.4s}] %{shortfunc}%{color:reset}: %{message} (%{shortfile})`,
	)

	// Configure console logger
	consoleBackend := logging.NewLogBackend(os.Stderr, "", 0)
	consoleBackendFormatter := logging.NewBackendFormatter(consoleBackend, format)
	consoleBackendLeveled := logging.AddModuleLevel(consoleBackendFormatter)
	consoleBackendLeveled.SetLevel(logLevel, "")
	backendList = append(backendList, consoleBackendLeveled)

	// Configure file logger
	if viper.IsSet("log.file") {
		var fileformat = logging.MustStringFormatter(
			`%{time:2006-01-02T15:04:05.000} [%{level:.4s}] %{shortfunc}: %{message}`,
		)

		f, err := os.OpenFile(viper.GetString("log.file"), os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
		if err != nil {
			log.Errorf("Error opening log file: %v", err)
		} else {
			filebackend := logging.NewLogBackend(f, "", 0)
			filebackendFormatter := logging.NewBackendFormatter(filebackend, fileformat)
			filebackendLeveled := logging.AddModuleLevel(filebackendFormatter)
			filebackendLeveled.SetLevel(logLevel, "")

			backendList = append(backendList, filebackendLeveled)
		}
	}

	// Configure syslog logger
	if viper.IsSet("log.syslog") && viper.GetBool("log.syslog") {
		syslogBackend, err := logging.NewSyslogBackend("")
		if err != nil {
			log.Errorf("Error with syslog: %v", err)
		}
		backendList = append(backendList, syslogBackend)
	}

	logging.SetBackend(backendList...)
}

// configureFlags configures all the command-line flags
func configureFlags() error {
	for _, o := range conf.Options {
		if err := util.AddOption(o, pflag.CommandLine); err != nil {
			return fmt.Errorf("failed to add option: %s", err)
		}
		if err := util.BindOption(o, pflag.CommandLine); err != nil {
			return fmt.Errorf("failed to bind option: %s", err)
		}
	}

	pflag.Usage = func() {
		fmt.Fprintln(os.Stderr, "Usage:")
		fmt.Fprintf(os.Stderr, "%s [flags]\n", os.Args[0])
		fmt.Fprintln(os.Stderr, "Flags:")
		pflag.PrintDefaults()
		fmt.Fprintln(os.Stderr, "\nConfiguration file:")
		fmt.Fprintf(os.Stderr, "Most options can be configured using a configuration file.\n")
		fmt.Fprintf(os.Stderr, "%s will look for a configuration file in the following locations:\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "  * at the path specified on the command line, if present\n")
		fmt.Fprintf(os.Stderr, "  * /etc/%s/%s.yaml\n", program, program)
		fmt.Fprintf(os.Stderr, "  * <PWD>/conf/%s.yaml\n", program)
		fmt.Fprintf(os.Stderr, "Options set on the command line will override options from the configuration file\n")
		fmt.Fprintln(os.Stderr, "")
	}

	pflag.Parse()

	// if --quiet, silence all output
	quietFlag := pflag.Lookup("quiet")
	if quietFlag != nil && quietFlag.Value.String() == "true" {
		viper.Set("verbose", 0)
	}

	return nil
}

// loadConfiguration reads config file and set default values
func loadConfiguration() {
	viper.SetConfigName(program)

	confPath := pflag.Lookup("conf")
	if confPath.Changed {
		viper.SetConfigFile(confPath.Value.String())
	}

	viper.AddConfigPath("/etc/" + program + "/")
	viper.AddConfigPath("conf/")

	err := viper.ReadInConfig()
	if err != nil {
		log.Error("could not read config file: ", err)
	}
}

// sigHandler manages OS signals:
// os.Interrupt and SIGTERM exits the program
// SIGHUP reloads the configuration
func sigHandler() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM, syscall.SIGHUP)

	for sig := range c {
		log.Debug("Received signal ", sig)

		switch sig {
		case syscall.SIGTERM, os.Interrupt:
			log.Info("Bye bye")
			os.Exit(0)
		case syscall.SIGHUP:
			log.Info("Reloading configuration file")
			loadConfiguration()
			// TODO: actually do something about this, like restart the server, etc.
		}
	}
}

func main() {
	// Command line management
	if err := configureFlags(); err != nil {
		log.Critical(err.Error())
		os.Exit(1)
	}

	loadConfiguration()
	configureLogger()

	jsonSettings, _ := json.Marshal(viper.AllSettings())
	log.Debugf("Configuration: %s", jsonSettings)

	versionString := fmt.Sprintf("%s %s-%s", program, version, githash)
	if viper.GetBool("version") {
		fmt.Println(versionString)
		os.Exit(0)
	}
	log.Infof(versionString)

	// Set number of parallel threads to number of CPUs.
	runtime.GOMAXPROCS(runtime.NumCPU())

	// manage signals
	go sigHandler()

	// Connect to DB
	db, err := db.NewSQLiteDB(viper.GetString("db.path"))
	if err != nil {
		log.Fatalf("couldn't connect to DB: %s", err)
	}
	log.Debugf("Connected to DB %s", viper.GetString("db.path"))

	// start API
	go server.Run(
		server.Configuration{
			BindAddress: viper.GetString("server.bindaddress"),
			BindPort:    viper.GetInt("server.port"),
			RestAddress: viper.GetString("server.restaddress"),
			RestPort:    viper.GetInt("server.restport"),
			Debug:       viper.GetBool("debug"),
		},
		db,
	)

	// start loop of crawling
	ticker := time.NewTicker(time.Minute * time.Duration(viper.GetInt("crawl.frequency")))
	go func() {
		for t := range ticker.C {
			log.Debugf("Ticker triggers a crawl at %s", t)
			feeds, err := db.ListFeeds(nil, false, 0, 0)
			if err != nil {
				log.Errorf("unable to list feeds: %s", err.Error())
			}

			for _, feed := range feeds {
				articles, err := db.CrawlArticles(feed)
				if err != nil {
					log.Errorf("unable to crawl articles for feed %s: %s", feed.String(), err.Error())
				}

				log.Debugf("%d articles crawled for feed %s", len(articles), feed.String())
			}
		}
	}()

	// infinite loop
	log.Info("Entering infinite loop")
	select {}
}
