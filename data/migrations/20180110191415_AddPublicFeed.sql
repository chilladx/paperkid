-- +goose Up
-- SQL in this section is executed when the migration is applied.

ALTER TABLE feed ADD COLUMN is_public BOOL DEFAULT '0'; 

CREATE INDEX feed_is_public_idx ON feed(is_public);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.

PRAGMA foreign_keys = OFF; 

CREATE TEMPORARY TABLE feed_TEMP (id,UUID,slug,title,description,url,base_url,ts_created,ts_updated,ts_deleted); 

INSERT INTO feed_TEMP SELECT id,UUID,slug,title,description,url,base_url,ts_created,ts_updated,ts_deleted FROM feed; 

DROP TABLE feed; 

CREATE TABLE feed
(
id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
UUID BLOB NOT NULL UNIQUE,
slug VARCHAR NOT NULL,
title VARCHAR NOT NULL,
description TEXT,
url VARCHAR NOT NULL UNIQUE,
base_url VARCHAR NOT NULL,
ts_created TIMESTAMP DEFAULT (strftime('%s', 'now')) NOT NULL,
ts_updated TIMESTAMP,
ts_deleted TIMESTAMP
); 

CREATE INDEX feed_id_idx ON feed(id); 

CREATE INDEX feed_slug_idx ON feed(slug); 

INSERT INTO feed (id,UUID,slug,title,description,url,base_url,ts_created,ts_updated,ts_deleted) SELECT id,UUID,slug,title,description,url,base_url,ts_created,ts_updated,ts_deleted FROM feed_TEMP; 

DROP TABLE feed_TEMP; 
