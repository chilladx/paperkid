-- +goose Up
-- SQL in this section is executed when the migration is applied.

CREATE TABLE user_feed
(
	feed INTEGER NOT NULL,
	user INTEGER NOT NULL,
	ts_subscribed TIMESTAMP DEFAULT (strftime('%s', 'now')) NOT NULL,
	FOREIGN KEY (feed) REFERENCES feed (id) ,
	FOREIGN KEY (user) REFERENCES user (id)
); 

CREATE INDEX user_feed_idx_1 ON user_feed (feed,user);
CREATE UNIQUE INDEX user_feed_cns_1 ON user_feed (feed,user);

INSERT INTO user_feed(feed, user, ts_subscribed) SELECT id, 0, ts_created FROM feed;

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.

DROP TABLE user_feed;