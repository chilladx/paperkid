
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE feed ADD COLUMN base_url VARCHAR NOT NULL DEFAULT " ";

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
CREATE TABLE IF NOT EXISTS feed_temp
(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    UUID BLOB NOT NULL UNIQUE,
    slug VARCHAR NOT NULL,
    title VARCHAR NOT NULL,
    description TEXT,
    url VARCHAR NOT NULL,
    ts_created TIMESTAMP DEFAULT (strftime('%s', 'now')) NOT NULL,
    ts_updated TIMESTAMP,
    ts_deleted TIMESTAMP
);

INSERT INTO feed_temp SELECT id,uuid,slug,title,description,url,ts_created,ts_updated,ts_deleted FROM feed;
DROP TABLE IF EXISTS feed;
ALTER TABLE feed_temp RENAME TO feed; 
