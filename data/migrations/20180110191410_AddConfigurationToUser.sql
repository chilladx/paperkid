-- +goose Up
-- SQL in this section is executed when the migration is applied.

ALTER TABLE user ADD COLUMN configuration BLOB DEFAULT '{}' NOT NULL;

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.

CREATE TABLE user_temp
(
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	authn_account_id INTEGER NOT NULL UNIQUE,
	UUID BLOB NOT NULL UNIQUE,
	email VARCHAR NOT NULL UNIQUE,
	ts_locked TIMESTAMP,
	ts_created TIMESTAMP DEFAULT (strftime('%s', 'now')) NOT NULL,
	ts_updated TIMESTAMP,
	ts_deleted TIMESTAMP
); 

INSERT INTO user_temp SELECT id,authn_account_id,UUID,email,ts_locked,ts_created,ts_updated,ts_deleted FROM user;

DROP TABLE IF EXISTS user;
DROP INDEX IF EXISTS user_id_idx;
DROP INDEX IF EXISTS user_authn_account_id_idx;

ALTER TABLE user_temp RENAME TO user;
CREATE INDEX user_id_idx ON user(id); 
CREATE INDEX user_authn_account_id_idx ON user(authn_account_id);
