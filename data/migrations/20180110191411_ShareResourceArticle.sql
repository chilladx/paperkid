-- +goose Up
-- SQL in this section is executed when the migration is applied.

/* -- replacing entire table (article) -- */; 

PRAGMA foreign_keys = OFF; 

CREATE TABLE user_article
(
	article INTEGER NOT NULL,
	user INTEGER NOT NULL,
	read BOOLEAN DEFAULT 'O' NOT NULL,
	FOREIGN KEY (article) REFERENCES article (id),
	FOREIGN KEY (user) REFERENCES user (id),
	UNIQUE(article, user) ON CONFLICT REPLACE
); 

CREATE INDEX user_article_idx_1 ON user_article (article,user);

INSERT INTO user_article SELECT id, 0, 1 FROM article WHERE read IS 1;

CREATE TEMPORARY TABLE article_SQLEDITOR_TEMP (id,UUID,feed,slug,title,url,body,checksum,ts_article,ts_created,ts_updated,ts_deleted); 

INSERT INTO article_SQLEDITOR_TEMP SELECT id,UUID,feed,slug,title,url,body,checksum,ts_article,ts_created,ts_updated,ts_deleted FROM article; 

DROP TABLE article; 

CREATE TABLE article
(
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	UUID BLOB NOT NULL UNIQUE,
	feed INTEGER NOT NULL,
	slug VARCHAR NOT NULL,
	title VARCHAR NOT NULL,
	url VARCHAR NOT NULL,
	body TEXT,
	checksum VARCHAR NOT NULL,
	ts_article TIMESTAMP,
	ts_created TIMESTAMP DEFAULT (strftime('%s', 'now')) NOT NULL,
	ts_updated TIMESTAMP,
	ts_deleted TIMESTAMP,
	FOREIGN KEY (feed) REFERENCES feed (id)
); 

CREATE INDEX article_id_idx ON article(id); 
CREATE INDEX article_slug_idx ON article(slug); 

INSERT INTO article (id,UUID,feed,slug,title,url,body,checksum,ts_article,ts_created,ts_updated,ts_deleted) SELECT id,UUID,feed,slug,title,url,body,checksum,ts_article,ts_created,ts_updated,ts_deleted FROM article_SQLEDITOR_TEMP; 
DROP TABLE article_SQLEDITOR_TEMP; 

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.

CREATE TEMPORARY TABLE article_SQLEDITOR_TEMP (id,UUID,feed,slug,title,url,body,checksum,ts_article,ts_created,ts_updated,ts_deleted); 

INSERT INTO article_SQLEDITOR_TEMP SELECT id,UUID,feed,slug,title,url,body,checksum,ts_article,ts_created,ts_updated,ts_deleted FROM article; 

DROP TABLE article; 

CREATE TABLE article
(
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	UUID BLOB NOT NULL UNIQUE,
	feed INTEGER NOT NULL,
	slug VARCHAR NOT NULL,
	title VARCHAR NOT NULL,
	url VARCHAR NOT NULL,
	body TEXT,
	checksum VARCHAR NOT NULL,
	read BOOLEAN DEFAULT '0' NOT NULL,
	ts_article TIMESTAMP,
	ts_created TIMESTAMP DEFAULT (strftime('%s', 'now')) NOT NULL,
	ts_updated TIMESTAMP,
	ts_deleted TIMESTAMP,
	FOREIGN KEY (feed) REFERENCES feed (id)
); 

CREATE INDEX article_id_idx ON article(id); 
CREATE INDEX article_slug_idx ON article(slug); 
CREATE INDEX article_read_idx ON article(read); 

INSERT INTO article (id,UUID,feed,slug,title,url,body,checksum,ts_article,ts_created,ts_updated,ts_deleted) SELECT id,UUID,feed,slug,title,url,body,checksum,ts_article,ts_created,ts_updated,ts_deleted FROM article_SQLEDITOR_TEMP; 
DROP TABLE article_SQLEDITOR_TEMP; 

UPDATE article SET read='1' WHERE id IN (SELECT article FROM user_article WHERE read='1');

DROP TABLE user_article;