-- +goose Up
-- SQL in this section is executed when the migration is applied.

CREATE TABLE user_feed_new
(
	feed INTEGER NOT NULL,
	user INTEGER NOT NULL,
	ts_subscribed TIMESTAMP DEFAULT (strftime('%s', 'now')),
	FOREIGN KEY (feed) REFERENCES feed (id) ,
	FOREIGN KEY (user) REFERENCES user (id)
); 

INSERT INTO user_feed_new(feed, user, ts_subscribed) SELECT feed, user, ts_subscribed FROM user_feed;
DROP TABLE user_feed;
ALTER TABLE user_feed_new RENAME TO user_feed;

CREATE INDEX user_feed_idx_1 ON user_feed (feed,user);
CREATE UNIQUE INDEX user_feed_cns_1 ON user_feed (feed,user);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.

CREATE TABLE user_feed_new
(
	feed INTEGER NOT NULL,
	user INTEGER NOT NULL,
	ts_subscribed TIMESTAMP DEFAULT (strftime('%s', 'now')) NOT NULL,
	FOREIGN KEY (feed) REFERENCES feed (id) ,
	FOREIGN KEY (user) REFERENCES user (id)
); 

INSERT INTO user_feed_new(feed, user, ts_subscribed) SELECT feed, user, ts_subscribed FROM user_feed;
DROP TABLE user_feed;
ALTER TABLE user_feed_new RENAME TO user_feed;

CREATE INDEX user_feed_idx_1 ON user_feed (feed,user);
CREATE UNIQUE INDEX user_feed_cns_1 ON user_feed (feed,user);
