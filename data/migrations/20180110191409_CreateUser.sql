-- +goose Up
-- SQL in this section is executed when the migration is applied.

CREATE TABLE user
(
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	authn_account_id INTEGER NOT NULL UNIQUE,
	UUID BLOB NOT NULL UNIQUE,
	email VARCHAR NOT NULL UNIQUE,
	ts_locked TIMESTAMP,
	ts_created TIMESTAMP DEFAULT (strftime('%s', 'now')) NOT NULL,
	ts_updated TIMESTAMP,
	ts_deleted TIMESTAMP
); 

CREATE INDEX user_id_idx ON user(id); 
CREATE INDEX user_authn_account_id_idx ON user(authn_account_id);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.

DROP INDEX user_id_idx;
DROP INDEX user_authn_account_id_idx;

DROP TABLE user;
