
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE IF NOT EXISTS feed
(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    UUID BLOB NOT NULL UNIQUE,
    slug VARCHAR NOT NULL,
    title VARCHAR NOT NULL,
    description TEXT,
    url VARCHAR NOT NULL,
    ts_created TIMESTAMP DEFAULT (strftime('%s', 'now')) NOT NULL,
    ts_updated TIMESTAMP,
    ts_deleted TIMESTAMP
);

CREATE TABLE IF NOT EXISTS article
(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    UUID BLOB NOT NULL UNIQUE,
    feed INTEGER NOT NULL,
    slug VARCHAR NOT NULL,
    title VARCHAR NOT NULL,
    url VARCHAR NOT NULL,
    body TEXT,
    checksum VARCHAR NOT NULL,
    ts_article TIMESTAMP,
    ts_created TIMESTAMP DEFAULT (strftime('%s', 'now')) NOT NULL,
    ts_updated TIMESTAMP,
    ts_deleted TIMESTAMP,
    FOREIGN KEY (feed) REFERENCES feed (id)
);

CREATE INDEX IF NOT EXISTS feed_id_idx ON feed(id);

CREATE INDEX IF NOT EXISTS feed_slug_idx ON feed(slug);

CREATE INDEX IF NOT EXISTS article_id_idx ON article(id);

CREATE INDEX IF NOT EXISTS article_slug_idx ON article(slug);


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

DROP INDEX IF EXISTS feed_id_idx;

DROP INDEX IF EXISTS feed_slug_idx;

DROP INDEX IF EXISTS article_id_idx;

DROP INDEX IF EXISTS article_slug_idx;

DROP TABLE IF EXISTS feed;
DROP TABLE IF EXISTS article;
