-- +goose Up
-- SQL in this section is executed when the migration is applied.

ALTER TABLE user ADD COLUMN role BLOB DEFAULT '{"roles":[]}' NOT NULL;

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.

CREATE TABLE user_TEMP
(
	id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	authn_account_id INTEGER NOT NULL UNIQUE,
	UUID BLOB NOT NULL UNIQUE,
	email VARCHAR NOT NULL UNIQUE,
	configuration BLOB DEFAULT '{}' NOT NULL,
	ts_locked TIMESTAMP,
	ts_created TIMESTAMP DEFAULT (strftime('%s', 'now')) NOT NULL,
	ts_updated TIMESTAMP,
	ts_deleted TIMESTAMP
); 

INSERT INTO user_TEMP SELECT id,authn_account_id,UUID,email,configuration,ts_locked,ts_created,ts_updated,ts_deleted FROM user;

DROP TABLE IF EXISTS user;
DROP INDEX IF EXISTS user_id_idx;
DROP INDEX IF EXISTS user_authn_account_id_idx;

ALTER TABLE user_TEMP RENAME TO user;
CREATE INDEX user_id_idx ON user(id); 
CREATE INDEX user_authn_account_id_idx ON user(authn_account_id);
