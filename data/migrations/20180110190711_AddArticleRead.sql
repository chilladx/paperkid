
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE article ADD COLUMN read BOOLEAN DEFAULT '0' NOT NULL; 
CREATE INDEX IF NOT EXISTS article_read_idx ON article(read);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
CREATE TABLE IF NOT EXISTS article_temp
(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    UUID BLOB NOT NULL UNIQUE,
    feed INTEGER NOT NULL,
    slug VARCHAR NOT NULL,
    title VARCHAR NOT NULL,
    url VARCHAR NOT NULL,
    body TEXT,
    checksum VARCHAR NOT NULL,
    ts_article TIMESTAMP,
    ts_created TIMESTAMP DEFAULT (strftime('%s', 'now')) NOT NULL,
    ts_updated TIMESTAMP,
    ts_deleted TIMESTAMP,
    FOREIGN KEY (feed) REFERENCES feed (id)
);

INSERT INTO article_temp SELECT id,UUID,feed,slug,title,url,body,checksum,ts_article,ts_created,ts_updated,ts_deleted FROM article;
DROP TABLE IF EXISTS article;
DROP INDEX IF EXISTS article_read_idx;
ALTER TABLE article_temp RENAME TO article; 
