INSERT INTO feed (UUID, slug, title, description, url, base_url)
VALUES
('f6e48ddb-e94c-4916-baf3-bf5dbe3a2fb3','slashdot', 'Slashdot', 'News for nerds, stuff that matters', 'http://rss.slashdot.org/Slashdot/slashdot', 'http://slashdot.org'),
('e5d01421-2318-4780-9e58-72991ec9b12d','gitlab', 'GitLab', '', 'https://about.gitlab.com/atom.xml', 'https://www.gitlab.com');
