package db

import (
	"database/sql/driver"
	"time"

	"github.com/google/uuid"
	logging "github.com/op/go-logging"
)

// PaperkidDB represents an abstract SQL database.
type PaperkidDB interface {
	GetFeed(uuid.UUID) (*Feed, error)
	ListFeeds(userID *int64, isPublic bool, pageSize int32, pageOffset uint64) ([]*Feed, error)
	SaveFeed(Feed) (*Feed, error)
	SubscribeFeed(Feed, *int64) (*Feed, error)
	UnsubscribeFeed(Feed, *int64) (*Feed, error)
	DeleteFeed(Feed) error

	GetArticle(uuid.UUID, uuid.UUID) (*Article, error)
	ListArticles(feedUUIDs []uuid.UUID, userID int64, unreadOnly bool, isPublic bool, pageSize int32, pageOffset uint64) ([]*Article, error)
	SaveArticle(Article, *int64) (*Article, error)
	CrawlArticles(*Feed) ([]Article, error)
	CountChecksumOccurence(uuid.UUID, string) (int, error)

	GetUser(uuid.UUID) (*User, error)
	GetUserByAuthIDAndEmail(int64, string) (*User, error)
	GetUserByAuthID(int64) (*User, error)
	ListUsers(pageSize int32, pageOffset uint64) ([]*User, error)
	SaveUser(User) (*User, error)
	DeleteUser(User) error
}

// Create a new instance of the logger
var log = logging.MustGetLogger("main")

// NullTime is usefull to get value from DB that might be null
type NullTime struct {
	Time  time.Time
	Valid bool // Valid is true if Time is not NULL
}

// Scan implements the Scanner interface.
func (nt *NullTime) Scan(value interface{}) error {
	nt.Time, nt.Valid = value.(time.Time)
	return nil
}

// Value implements the driver Valuer interface.
func (nt NullTime) Value() (driver.Value, error) {
	if !nt.Valid {
		return nil, nil
	}
	return nt.Time, nil
}

// NullBytes is useful to get value from DB that might be null
type NullBytes struct {
	Bytes []byte
	Valid bool
}

// Scan implements the Scanner interface.
func (nb *NullBytes) Scan(value interface{}) error {
	nb.Bytes, nb.Valid = value.([]byte)
	return nil
}

// Value implements the driver Valuer interface.
func (nb NullBytes) Value() (driver.Value, error) {
	if !nb.Valid {
		return nil, nil
	}
	return nb.Bytes, nil
}

// NullBoolean is useful to get value from DB that might be null
type NullBoolean struct {
	Boolean bool
	Valid   bool
}

// Scan implements the Scanner interface.
func (nb *NullBoolean) Scan(value interface{}) error {
	nb.Boolean, nb.Valid = value.(bool)
	return nil
}

// Value implements the driver Valuer interface.
func (nb NullBoolean) Value() (driver.Value, error) {
	if !nb.Valid {
		return nil, nil
	}
	return nb.Boolean, nil
}

// row is a helper interface to encapsulate sql.Row and sql.Rows
type row interface {
	Scan(dest ...interface{}) error
}
