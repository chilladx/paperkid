package db

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/SlyMarbo/rss"
	"github.com/google/uuid"
)

// A Feed is the representation of a feed in the DB
type Feed struct {
	ID           int64
	UUID         uuid.UUID
	Slug         string
	Title        string
	Description  string
	URL          string
	BaseURL      string
	IsPublic     bool
	TsSubscribed time.Time
	TsCreated    time.Time
	TsUpdated    time.Time
	TsDeleted    time.Time
}

// GetFeed searches for a Feed in the DB based on its UUID
func (db *SQLiteDB) GetFeed(feedUUID uuid.UUID) (*Feed, error) {
	f, err := scanFeed(db.client.QueryRow(`
        SELECT id, uuid, slug, title, description, url, base_url, is_public, NULL, ts_created, ts_updated, ts_deleted
        FROM feed
        WHERE uuid = ? AND ts_deleted IS NULL`,
		feedUUID))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve Feed: %s", err)
	}

	return f, nil
}

// ListFeeds returns all the Feeds from the DB
func (db *SQLiteDB) ListFeeds(userID *int64, isPublic bool, pageSize int32, pageOffset uint64) ([]*Feed, error) {
	args := []interface{}{}
	req := new(strings.Builder)

	if userID != nil {
		fmt.Fprint(req, `
	        SELECT id, uuid, slug, title, description, url, base_url, is_public, ts_subscribed, ts_created, ts_updated, ts_deleted
	        FROM feed
	        LEFT OUTER JOIN user_feed ON feed.id=user_feed.feed
	        WHERE ts_deleted IS NULL AND ts_subscribed IS NOT NULL AND (user_feed.user=?)`)
		args = append(args, userID)
	} else {
		fmt.Fprint(req, `
	        SELECT id, uuid, slug, title, description, url, base_url, is_public, NULL, ts_created, ts_updated, ts_deleted
	        FROM feed
			WHERE ts_deleted IS NULL`)
		if isPublic {
			fmt.Fprint(req, ` AND is_public IS TRUE`)
		}
	}

	if pageSize > 0 {
		fmt.Fprintf(req, " LIMIT %d", pageSize)
	}
	if pageOffset > 0 {
		fmt.Fprintf(req, " OFFSET %d", pageOffset)
	}

	rows, err := db.client.Query(req.String(), args...)
	if err != nil {
		return nil, fmt.Errorf("Error running the query: %s", err.Error())
	}
	defer rows.Close()

	feeds := make([]*Feed, 0)
	for rows.Next() {
		curfeed, err := scanFeed(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		feeds = append(feeds, curfeed)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*Feed{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return feeds, nil
}

// SaveFeed writes the Feed object in the database
func (db *SQLiteDB) SaveFeed(f Feed) (*Feed, error) {
	if (f.UUID == uuid.UUID{}) {
		f.UUID = uuid.New()
	}

	if f.ID == 0 {
		stmt, err := db.client.Prepare(`
            INSERT INTO feed (uuid, slug, title, description, url, base_url, is_public)
            VALUES (?, ?, ?, ?, ?, ?, ?)`)
		if err != nil {
			return nil, fmt.Errorf("Unable to prepare SQL statement: %s", err.Error())
		}
		defer stmt.Close()

		result, err := stmt.Exec(f.UUID, f.Slug, f.Title, f.Description, f.URL, f.BaseURL, f.IsPublic)
		if err != nil {
			return nil, fmt.Errorf("Unable to insert feed: %s", err.Error())
		}

		f.ID, err = result.LastInsertId()
		if err != nil {
			return nil, fmt.Errorf("Unable to get last ID after insert: %s", err.Error())
		}
	} else {
		stmt, err := db.client.Prepare(`
            UPDATE feed
            SET slug=?, title=?, description=?, url=?, base_url=?, is_public=?, ts_updated=strftime('%s', 'now')
            WHERE id=?;`)
		if err != nil {
			return nil, fmt.Errorf("Unable to prepare SQL statement: %s", err.Error())
		}
		defer stmt.Close()

		_, err = stmt.Exec(f.Slug, f.Title, f.Description, f.URL, f.BaseURL, f.IsPublic, f.ID)
		if err != nil {
			return nil, fmt.Errorf("Unable to update feed: %s", err.Error())
		}
	}

	return db.GetFeed(f.UUID)
}

// SubscribeFeed links the Feed object to the user in the database
func (db *SQLiteDB) SubscribeFeed(f Feed, userID *int64) (*Feed, error) {
	if (f.UUID == uuid.UUID{}) {
		return nil, fmt.Errorf("Missing feed")
	}
	if userID == nil {
		return nil, fmt.Errorf("Missing user")
	}

	stmt, err := db.client.Prepare(`
        INSERT INTO user_feed (feed, user)
        VALUES ((SELECT id FROM feed WHERE uuid=?), ?)
        ON CONFLICT(feed, user) DO
        	UPDATE SET ts_subscribed=strftime('%s', 'now')
        	WHERE feed=(SELECT id FROM feed WHERE uuid=?) AND user=?`)
	if err != nil {
		return nil, fmt.Errorf("Unable to prepare SQL statement: %s", err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec(f.UUID, userID, f.UUID, userID)
	if err != nil {
		return nil, fmt.Errorf("Unable to insert user_feed: %s", err.Error())
	}

	return db.GetFeed(f.UUID)
}

// UnsubscribeFeed unlinks the Feed object from the user in the database
func (db *SQLiteDB) UnsubscribeFeed(f Feed, userID *int64) (*Feed, error) {
	if (f.UUID == uuid.UUID{}) {
		return nil, fmt.Errorf("Missing feed")
	}
	if userID == nil {
		return nil, fmt.Errorf("Missing user")
	}

	stmt, err := db.client.Prepare(`
        UPDATE user_feed
        SET ts_subscribed=NULL
    	WHERE feed=(SELECT id FROM feed WHERE uuid=?) AND user=?`)
	if err != nil {
		return nil, fmt.Errorf("Unable to prepare SQL statement: %s", err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec(f.UUID, userID)
	if err != nil {
		return nil, fmt.Errorf("Unable to remove user_feed: %s", err.Error())
	}

	return db.GetFeed(f.UUID)
}

// DeleteFeed deletes the Feed object in the database
func (db *SQLiteDB) DeleteFeed(f Feed) error {
	if f.ID == 0 {
		return fmt.Errorf("Unknown Feed: missing ID")
	}

	relatedArticles, err := db.ListArticles([]uuid.UUID{f.UUID}, 0, false, false, 0, 0)
	if err != nil {
		log.Errorf("unable to list articles from feed %s: %s", f.UUID.String(), err)
	}
	for _, article := range relatedArticles {
		err = db.DeleteArticle(*article)
		if err != nil {
			log.Errorf("unable to delete article %s: %s", article.String(), err.Error())
		}
	}

	_, err = db.client.Exec(`DELETE FROM user_feed WHERE feed=?;`, f.ID)
	if err != nil {
		return fmt.Errorf("Unable to unsubscribe users from feed: %s", err.Error())
	}

	stmt, err := db.client.Prepare(`
        UPDATE feed
        SET ts_deleted=strftime('%s', 'now')
        WHERE id=?;`)
	if err != nil {
		return fmt.Errorf("Unable to prepare SQL statement: %s", err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec(f.ID)
	if err != nil {
		return fmt.Errorf("Unable to delete feed: %s", err.Error())
	}

	return nil
}

// PingFeed tests the URL of a Feed from the online resource
func (f *Feed) PingFeed() error {
	if f.URL == "" {
		return fmt.Errorf("Missing mandatory URL from feed")
	}

	_, err := rss.Fetch(f.URL)
	if err != nil {
		return fmt.Errorf("unable to fetch the URL (%s): %s", f.URL, err.Error())
	}

	return nil
}

// GrabDescription gets the description of a Feed from the online resource
func (f *Feed) GrabDescription() error {
	if f.URL == "" {
		return fmt.Errorf("Missing mandatory URL from feed")
	}

	rssFeed, err := rss.Fetch(f.URL)
	if err != nil {
		return fmt.Errorf("unable to fetch the URL (%s): %s", f.URL, err.Error())
	}

	f.Description = rssFeed.Description

	return nil
}

func (f *Feed) String() string {
	return fmt.Sprintf("[%s] %s", f.UUID.String(), f.Title)
}

func scanFeed(r row) (*Feed, error) {
	var f Feed
	var tsSubscribed, tsCreated, tsUpdated, tsDeleted NullTime

	err := r.Scan(
		&f.ID,
		&f.UUID,
		&f.Slug,
		&f.Title,
		&f.Description,
		&f.URL,
		&f.BaseURL,
		&f.IsPublic,
		&tsSubscribed,
		&tsCreated,
		&tsUpdated,
		&tsDeleted,
	)
	if err != nil {
		return nil, fmt.Errorf("Feed not found: %s", err.Error())
	}

	if tsSubscribed.Valid {
		f.TsSubscribed = tsSubscribed.Time
	} else {
		f.TsSubscribed = time.Time{}
	}
	if tsCreated.Valid {
		f.TsCreated = tsCreated.Time
	} else {
		f.TsCreated = time.Time{}
	}
	if tsUpdated.Valid {
		f.TsUpdated = tsUpdated.Time
	} else {
		f.TsUpdated = time.Time{}
	}
	if tsDeleted.Valid {
		f.TsDeleted = tsDeleted.Time
	} else {
		f.TsDeleted = time.Time{}
	}

	return &f, nil
}
