package db

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/google/uuid"
	"gitlab.com/chilladx/paperkid/api"
)

// A User is the representation of a user in the DB
type User struct {
	ID            int64
	AuthID        int64
	UUID          uuid.UUID
	Email         string
	Configuration []byte
	Role          []UserRole
	TsLocked      time.Time
	TsCreated     time.Time
	TsUpdated     time.Time
	TsDeleted     time.Time
}

// UserRole is a private type to represent api.User value
type UserRole int

// UserRole constants
const (
	UserRoleUnknown = UserRole(api.User_UNKNOWN)
	UserRoleAdmin   = UserRole(api.User_ADMIN)
	UserRoleUser    = UserRole(api.User_USER)
)

func (ur UserRole) String() string {
	switch ur {
	case UserRoleUnknown:
		return "unknown"
	case UserRoleAdmin:
		return "admin"
	case UserRoleUser:
		return "user"
	default:
		panic(fmt.Sprintf("%d is not a valid UserRole", int(ur)))
	}
}

// local type to encode the list of roles in the DB
type dbRole struct {
	Roles []int `json:"roles"`
}

// ListUsers returns all the Users from the DB
func (db *SQLiteDB) ListUsers(pageSize int32, pageOffset uint64) ([]*User, error) {
	args := []interface{}{}
	req := new(strings.Builder)

	fmt.Fprint(req, `
        SELECT id, authn_account_id, uuid, email, configuration, role, ts_locked, ts_created, ts_updated, ts_deleted
        FROM user
        WHERE ts_deleted IS NULL`)

	if pageSize > 0 {
		fmt.Fprintf(req, " LIMIT %d", pageSize)
	}
	if pageOffset > 0 {
		fmt.Fprintf(req, " OFFSET %d", pageOffset)
	}

	rows, err := db.client.Query(req.String(), args...)
	if err != nil {
		return nil, fmt.Errorf("Error running the query: %s", err.Error())
	}
	defer rows.Close()

	users := make([]*User, 0)
	for rows.Next() {
		curuser, err := scanUser(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		users = append(users, curuser)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*User{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return users, nil
}

// GetUser searches for a User in the DB based on its UUID
func (db *SQLiteDB) GetUser(userUUID uuid.UUID) (*User, error) {
	f, err := scanUser(db.client.QueryRow(`
        SELECT id, authn_account_id, uuid, email, configuration, role, ts_locked, ts_created, ts_updated, ts_deleted
        FROM user
        WHERE uuid = ? AND ts_deleted IS NULL`,
		userUUID))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve User: %s", err)
	}

	return f, nil
}

// GetUserByAuthIDAndEmail searches for a User in the DB based on its AuthID
func (db *SQLiteDB) GetUserByAuthIDAndEmail(authID int64, email string) (*User, error) {
	f, err := scanUser(db.client.QueryRow(`
        SELECT id, authn_account_id, uuid, email, configuration, role, ts_locked, ts_created, ts_updated, ts_deleted
        FROM user
        WHERE authn_account_id = ? AND email = ? AND ts_deleted IS NULL`,
		authID, email))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve User: %s", err)
	}

	return f, nil
}

// GetUserByAuthID searches for a User in the DB based on its AuthID
func (db *SQLiteDB) GetUserByAuthID(authID int64) (*User, error) {
	f, err := scanUser(db.client.QueryRow(`
        SELECT id, authn_account_id, uuid, email, configuration, role, ts_locked, ts_created, ts_updated, ts_deleted
        FROM user
        WHERE authn_account_id = ? AND ts_deleted IS NULL`,
		authID))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("failed to retrieve User: %s", err)
	}

	return f, nil
}

// SaveUser writes the User object in the database
func (db *SQLiteDB) SaveUser(u User) (*User, error) {
	if (u.UUID == uuid.UUID{}) {
		u.UUID = uuid.New()
	}

	var listRoles dbRole
	for _, r := range u.Role {
		listRoles.Roles = append(listRoles.Roles, int(r))
	}
	roles, err := json.Marshal(listRoles)
	if err != nil {
		return nil, fmt.Errorf("couldn't marshal Role to JSON: %s", err)
	}

	if u.ID == 0 {
		stmt, err := db.client.Prepare(`
            INSERT INTO user (authn_account_id, uuid, email, configuration, role)
            VALUES (?, ?, ?, ?, ?)`)
		if err != nil {
			return nil, fmt.Errorf("Unable to prepare SQL statement: %s", err.Error())
		}
		defer stmt.Close()

		result, err := stmt.Exec(u.AuthID, u.UUID, u.Email, u.Configuration, roles)
		if err != nil {
			return nil, fmt.Errorf("Unable to insert user: %s", err.Error())
		}

		u.ID, err = result.LastInsertId()
		if err != nil {
			return nil, fmt.Errorf("Unable to get last ID after insert: %s", err.Error())
		}
	} else {
		args := []interface{}{}
		req := new(strings.Builder)

		fmt.Fprint(req, `UPDATE user SET configuration=?, role=?, ts_updated=?`)
		args = append(args, u.Configuration, roles, time.Now().Unix())

		if !u.TsLocked.IsZero() && u.TsLocked.Unix() > 0 {
			fmt.Fprint(req, ", ts_locked=?")
			args = append(args, u.TsLocked.Unix())
		} else {
			fmt.Fprint(req, ", ts_locked=NULL")
		}

		fmt.Fprint(req, " WHERE id=?;")
		args = append(args, u.ID)

		log.Debugf("Updating user: %s / %s", req.String(), args)
		_, err = db.client.Exec(req.String(), args...)
		if err != nil {
			return nil, fmt.Errorf("Unable to update user: %s", err.Error())
		}
	}

	return db.GetUser(u.UUID)
}

// DeleteUser deletes the User object in the database
func (db *SQLiteDB) DeleteUser(u User) error {
	if u.ID == 0 {
		return fmt.Errorf("Unknown User: missing ID")
	}

	stmt, err := db.client.Prepare(`
        UPDATE user
        SET ts_deleted=strftime('%s', 'now')
        WHERE id=?;`)
	if err != nil {
		return fmt.Errorf("Unable to prepare SQL statement: %s", err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec(u.ID)
	if err != nil {
		return fmt.Errorf("Unable to delete user: %s", err.Error())
	}

	return nil
}

func (u *User) String() string {
	return fmt.Sprintf("[%s] %s", u.UUID.String(), u.Email)
}

func scanUser(r row) (*User, error) {
	var u User
	var tsLocked, tsCreated, tsUpdated, tsDeleted NullTime
	var configuration, role NullBytes

	err := r.Scan(
		&u.ID,
		&u.AuthID,
		&u.UUID,
		&u.Email,
		&configuration,
		&role,
		&tsLocked,
		&tsCreated,
		&tsUpdated,
		&tsDeleted,
	)
	if err != nil {
		return nil, fmt.Errorf("User not found: %s", err.Error())
	}

	if configuration.Valid {
		u.Configuration = configuration.Bytes
	} else {
		return nil, fmt.Errorf("Configuration template is invalid")
	}
	if role.Valid {
		var ur dbRole

		if err = json.Unmarshal(role.Bytes, &ur); err != nil {
			return nil, fmt.Errorf("couldn't unmarshal Roles: %s", err)
		}

		for _, r := range ur.Roles {
			u.Role = append(u.Role, UserRole(r))
		}
		if len(u.Role) == 0 {
			u.Role = []UserRole{UserRoleUnknown}
		}
	} else {
		return nil, fmt.Errorf("Role is invalid")
	}
	if tsLocked.Valid {
		u.TsLocked = tsLocked.Time
	} else {
		u.TsLocked = time.Time{}
	}
	if tsCreated.Valid {
		u.TsCreated = tsCreated.Time
	} else {
		u.TsCreated = time.Time{}
	}
	if tsUpdated.Valid {
		u.TsUpdated = tsUpdated.Time
	} else {
		u.TsUpdated = time.Time{}
	}
	if tsDeleted.Valid {
		u.TsDeleted = tsDeleted.Time
	} else {
		u.TsDeleted = time.Time{}
	}

	return &u, nil
}
