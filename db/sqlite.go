package db

import (
	"database/sql"

	// import SQLite just to reference driver
	_ "github.com/mattn/go-sqlite3"
)

// SQLiteDB is a concrete implementation of `sql.DB`
// that uses Sqlite as a SQL database.
type SQLiteDB struct {
	client *sql.DB
	path   string
}

// NewSQLiteDB creates a new connection to an Sqlite DB
// The `path` argument is the path to the database
// file.
func NewSQLiteDB(path string) (PaperkidDB, error) {
	var err error
	var sqldb SQLiteDB
	sqldb.client, err = sql.Open("sqlite3", path)
	if err != nil {
		return nil, err
	}

	sqldb.path = path

	return &sqldb, nil
}

// NewSQLiteDBTest creates a fake connection to an mock DB
// The `client` argument is the handler for the mock.
func NewSQLiteDBTest(client *sql.DB) (PaperkidDB, error) {
	var sqldb SQLiteDB

	sqldb.client = client
	sqldb.path = ""

	return &sqldb, nil
}
