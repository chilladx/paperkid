package db

import (
	"crypto/sha1"
	"database/sql"
	"encoding/base64"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/SlyMarbo/rss"
	"github.com/google/uuid"
	"github.com/gosimple/slug"
)

// An Article is the representation of an article in the DB
type Article struct {
	ID        int64
	UUID      uuid.UUID
	Feed      int64
	FeedUUID  uuid.UUID
	Slug      string
	Title     string
	URL       string
	Body      string
	Checksum  string
	Read      bool
	TsArticle time.Time
	TsCreated time.Time
	TsUpdated time.Time
	TsDeleted time.Time
}

// GetArticle searches for an Article in the DB based on its UUID
func (db *SQLiteDB) GetArticle(feedUUID, articleUUID uuid.UUID) (*Article, error) {
	i, err := scanArticle(db.client.QueryRow(`
        SELECT i.id, i.uuid, i.feed, f.uuid, i.slug, i.title, i.url, i.body, i.checksum, user_article.read, i.ts_article, i.ts_created, i.ts_updated, i.ts_deleted
        FROM article AS i, feed AS f
        LEFT OUTER JOIN user_article ON i.id=user_article.article
        WHERE i.feed=f.id AND i.uuid = ? AND f.uuid = ? AND i.ts_deleted IS NULL`,
		articleUUID, feedUUID))
	if err != nil {
		if err == sql.ErrNoRows {
			// Return nil, nil to indicate no error but no result (404 Not Found)
			return nil, nil
		}
		return nil, fmt.Errorf("Article not found: %s", err.Error())
	}

	return i, nil
}

// ListArticles returns the Articles from the DB (with a filter on a specific Feed)
func (db *SQLiteDB) ListArticles(feedUUIDs []uuid.UUID, userID int64, unreadOnly bool, isPublic bool, pageSize int32, pageOffset uint64) ([]*Article, error) {
	args := []interface{}{}
	req := new(strings.Builder)

	if userID == 0 {
		fmt.Fprint(req, `
	        SELECT i.id, i.uuid, i.feed, f.uuid, i.slug, i.title, i.url, i.body, i.checksum, '0', i.ts_article, i.ts_created, i.ts_updated, i.ts_deleted
	        FROM article AS i, feed AS f
			WHERE i.feed=f.id AND i.ts_deleted IS NULL`)

		if isPublic {
			fmt.Fprint(req, ` AND f.is_public IS TRUE`)
		}
	} else {
		fmt.Fprint(req, `
	        SELECT i.id, i.uuid, i.feed, f.uuid, i.slug, i.title, i.url, i.body, i.checksum, user_article.read, i.ts_article, i.ts_created, i.ts_updated, i.ts_deleted
	        FROM article AS i, feed AS f
	        LEFT OUTER JOIN user_article ON i.id=user_article.article
	        WHERE i.feed=f.id AND i.ts_deleted IS NULL AND (user_article.user=? OR user_article.user IS NULL) AND i.feed IN (SELECT feed FROM user_feed WHERE ts_subscribed IS NOT NULL)`)
		args = append(args, userID)

		if unreadOnly {
			fmt.Fprintf(req, ` AND (user_article.read=FALSE OR user_article.read IS NULL)`)
		}
	}

	if len(feedUUIDs) > 0 {
		s := strings.Repeat("?,", len(feedUUIDs))
		fmt.Fprintf(req, " AND f.UUID IN (%s)", s[:len(s)-1])
		for _, u := range feedUUIDs {
			args = append(args, u)
		}
	}

	fmt.Fprintf(req, ` ORDER BY i.ts_created DESC`)

	if pageSize > 0 {
		fmt.Fprintf(req, " LIMIT %d", pageSize)
	}
	if pageOffset > 0 {
		fmt.Fprintf(req, " OFFSET %d", pageOffset)
	}

	rows, err := db.client.Query(req.String(), args...)
	if err != nil {
		return nil, fmt.Errorf("Error running the query: %s", err.Error())
	}
	defer rows.Close()

	articles := make([]*Article, 0)
	for rows.Next() {
		curarticle, err := scanArticle(rows)
		if err != nil {
			log.Errorf("unable to scan row: %s", err)
			continue
		}

		articles = append(articles, curarticle)
	}
	err = rows.Err()
	if err != nil {
		if err == sql.ErrNoRows {
			return []*Article{}, nil
		}
		log.Errorf("issue in the query: %s", err)
	}

	return articles, nil
}

// SaveArticle writes the Article object in the database
func (db *SQLiteDB) SaveArticle(a Article, userID *int64) (*Article, error) {
	if a.ID == 0 {
		// It's a creation of an object in the DB
		if (a.UUID == uuid.UUID{}) {
			a.UUID = uuid.New()
		}

		stmt, err := db.client.Prepare(`
	        INSERT INTO article (uuid, feed, slug, title, url, body, checksum, ts_article)
	        VALUES (?,
	        	(SELECT id FROM feed WHERE uuid=? AND ts_deleted IS NULL),
	        	?, ?, ?, ?, ?, ?)`)
		if err != nil {
			return nil, fmt.Errorf("Unable to prepare SQL statement: %s", err.Error())
		}
		defer stmt.Close()

		result, err := stmt.Exec(
			a.UUID,
			a.FeedUUID,
			a.Slug,
			a.Title,
			a.URL,
			a.Body,
			a.Checksum,
			a.TsArticle,
		)
		if err != nil {
			return nil, fmt.Errorf("Unable to insert article: %s", err.Error())
		}

		a.ID, err = result.LastInsertId()
		if err != nil {
			return nil, fmt.Errorf("Unable to get last ID after insert: %s", err.Error())
		}
	} else {
		// It's an update of an object in the DB
		stmt, err := db.client.Prepare(`
	        UPDATE article
	        SET feed=(SELECT id FROM feed WHERE uuid=? AND ts_deleted IS NULL), slug=?, title=?, url=?, body=?, checksum=?, ts_updated=strftime('%s', 'now')
	        WHERE id=?`)
		if err != nil {
			return nil, fmt.Errorf("Unable to prepare SQL statement: %s", err.Error())
		}
		defer stmt.Close()

		rssChecksum := calculateChecksum(a.Body)

		_, err = stmt.Exec(
			a.FeedUUID,
			slug.Make(a.Title),
			a.Title,
			a.URL,
			a.Body,
			rssChecksum,
			a.ID,
		)
		if err != nil {
			return nil, fmt.Errorf("Unable to update article: %s", err.Error())
		}

		// Update link table with user
		if userID != nil {
			stmtUA, err := db.client.Prepare(`
		        INSERT INTO user_article(article, user, read)
		        VALUES (?, ?, ?)
		        ON CONFLICT(article, user) DO
		        	UPDATE SET read=?
		        	WHERE article=? AND user=?`)
			if err != nil {
				return nil, fmt.Errorf("Unable to prepare SQL statement: %s", err.Error())
			}
			defer stmtUA.Close()

			_, err = stmtUA.Exec(
				a.ID,
				userID,
				a.Read,
				a.Read,
				a.ID,
				userID,
			)
			if err != nil {
				return nil, fmt.Errorf("Unable to update article: %s", err.Error())
			}
		}
	}

	return db.GetArticle(a.FeedUUID, a.UUID)
}

// CountChecksumOccurence counts how many occurence of a checksum exists in the DB
func (db *SQLiteDB) CountChecksumOccurence(feedUUID uuid.UUID, checksum string) (int, error) {
	stmt, err := db.client.Prepare(`
        SELECT count(id)
        FROM article
        WHERE checksum = ?
        AND feed = (
        	SELECT id FROM feed WHERE uuid=? AND ts_deleted IS NULL )`)
	if err != nil {
		return 0, fmt.Errorf("Unable to prepare SQL statement: %s", err.Error())
	}
	defer stmt.Close()

	var count int

	err = stmt.QueryRow(checksum, feedUUID).Scan(&count)
	if err != nil {
		return 0, fmt.Errorf("Article not found: %s", err.Error())
	}

	return count, nil
}

// CrawlArticles parses the Feed and create corresponding Articles in the database
func (db *SQLiteDB) CrawlArticles(f *Feed) ([]Article, error) {
	rssFeed, err := rss.Fetch(f.URL)
	if err != nil {
		return nil, fmt.Errorf("unable to fetch the URL (%s): %s", f.URL, err.Error())
	}

	articles := make([]Article, 0)

	for _, rssArticle := range rssFeed.Items {
		var myBody string

		if rssArticle.Content == "" {
			myBody = rssArticle.Summary
		} else {
			myBody = rssArticle.Content
		}

		myBody = cleanLinks(myBody, f.BaseURL)
		rssChecksum := calculateChecksum(myBody)

		dupe, err := db.CountChecksumOccurence(f.UUID, rssChecksum)
		if err != nil {
			log.Errorf("unable to determine duplicates for article: %s", err)
			continue
		}
		if dupe > 0 {
			log.Debugf("Duplicate checksum (%s) found: %d", rssChecksum, dupe)
			continue
		}

		myArticle := Article{
			Feed:      f.ID,
			FeedUUID:  f.UUID,
			Slug:      slug.Make(rssArticle.Title),
			Title:     rssArticle.Title,
			URL:       rssArticle.Link,
			Body:      myBody,
			Checksum:  rssChecksum,
			Read:      false,
			TsArticle: rssArticle.Date,
		}

		dbArticle, err := db.SaveArticle(myArticle, nil)
		if err != nil {
			return nil, fmt.Errorf("unable to save Article (%s): %s", rssArticle.String(), err.Error())
		}

		articles = append(articles, *dbArticle)
	}

	return articles, nil
}

// DeleteArticle deletes the Article object in the database
func (db *SQLiteDB) DeleteArticle(i Article) error {
	if i.ID == 0 {
		return fmt.Errorf("Unknown Article: missing ID")
	}
	stmt, err := db.client.Prepare(`
        UPDATE article
        SET ts_deleted=strftime('%s', 'now')
        WHERE id=?
        	AND feed = (
        		SELECT id
        		FROM feed
        		WHERE uuid = ?
        			AND ts_deleted IS NULL);`)
	if err != nil {
		return fmt.Errorf("Unable to prepare SQL statement: %s", err.Error())
	}
	defer stmt.Close()

	_, err = stmt.Exec(i.ID, i.FeedUUID)
	if err != nil {
		return fmt.Errorf("Unable to delete article: %s", err.Error())
	}

	return nil
}

// calculateChecksum returns the checksum of the string with SHA-1
func calculateChecksum(s string) string {
	hasher := sha1.New()
	io.WriteString(hasher, s)
	return base64.URLEncoding.EncodeToString(hasher.Sum(nil))
}

// cleanLinks transforms the relative links in absolute links in input string
func cleanLinks(src, base string) string {
	src = strings.Replace(src, "src=\"/", "src=\""+base+"/", -1)
	src = strings.Replace(src, "src='/", "src='"+base+"/", -1)
	src = strings.Replace(src, "href=\"/", "href=\""+base+"/", -1)
	src = strings.Replace(src, "href='/", "href='"+base+"/", -1)

	return src
}

func (i *Article) String() string {
	return fmt.Sprintf("[%s] %s", i.UUID.String(), i.Title)
}

func scanArticle(r row) (*Article, error) {
	var i Article
	var tsArticle, tsCreated, tsUpdated, tsDeleted NullTime
	var articleRead NullBoolean

	err := r.Scan(
		&i.ID,
		&i.UUID,
		&i.Feed,
		&i.FeedUUID,
		&i.Slug,
		&i.Title,
		&i.URL,
		&i.Body,
		&i.Checksum,
		&articleRead,
		&tsArticle,
		&tsCreated,
		&tsUpdated,
		&tsDeleted,
	)
	if err != nil {
		return nil, fmt.Errorf("Article not found: %s", err.Error())
	}

	if articleRead.Valid {
		i.Read = articleRead.Boolean
	} else {
		i.Read = false
	}
	if tsArticle.Valid {
		i.TsArticle = tsArticle.Time
	} else {
		i.TsArticle = time.Time{}
	}
	if tsCreated.Valid {
		i.TsCreated = tsCreated.Time
	} else {
		i.TsCreated = time.Time{}
	}
	if tsUpdated.Valid {
		i.TsUpdated = tsUpdated.Time
	} else {
		i.TsUpdated = time.Time{}
	}
	if tsDeleted.Valid {
		i.TsDeleted = tsDeleted.Time
	} else {
		i.TsDeleted = time.Time{}
	}

	return &i, nil
}
