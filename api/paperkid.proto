syntax = "proto3";
package api;

import "google/api/annotations.proto";
import "google/protobuf/timestamp.proto";
import "google/protobuf/empty.proto";
import "google/protobuf/field_mask.proto";

service Paperkid {
  rpc ListFeeds(ListFeedsRequest) returns (ListFeedsResponse) {
    option (google.api.http) = {
      get: "/v1/feeds"
    };
  }

  rpc GetFeed(GetFeedRequest) returns (Feed) {
    option (google.api.http) = {
      get: "/v1/{name=feeds/*}"
    };
  }

  rpc CreateFeed(CreateFeedRequest) returns (Feed) {
    option (google.api.http) = {
      post: "/v1/feeds"
      body: "*"
    };
  }

  rpc UpdateFeed(UpdateFeedRequest) returns (Feed) {
    option (google.api.http) = {
      put: "/v1/{feed.name=feeds/*}"
      body: "*"
    };
  }

  rpc SubscribeFeed(SubscribeFeedRequest) returns (Feed) {
    option (google.api.http) = {
      post: "/v1/{feed.name=feeds/*}/subscribe"
      body: "*"
    };
  }

  rpc UnsubscribeFeed(UnsubscribeFeedRequest) returns (Feed) {
    option (google.api.http) = {
      post: "/v1/{feed.name=feeds/*}/unsubscribe"
      body: "*"
    };
  }

  rpc DeleteFeed(DeleteFeedRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {
      delete: "/v1/{name=feeds/*}"
    };
  }

  rpc ListArticles(ListArticlesRequest) returns (ListArticlesResponse) {
    option (google.api.http) = {
      get: "/v1/{parent=feeds/*}/articles"
    };
  }

  rpc ListAllArticles(ListArticlesRequest) returns (ListArticlesResponse) {
    option (google.api.http) = {
      get: "/v1/articles"
    };
  }

  rpc QueryArticles(QueryArticlesRequest) returns (QueryArticlesResponse) {
    option (google.api.http) = {
      post: "/v1/articles"
      body: "*"
    };
  }

  rpc GetArticle(GetArticleRequest) returns (Article) {
    option (google.api.http) = {
      get: "/v1/{name=feeds/*/articles/*}"
    };
  }

  rpc UpdateArticle(UpdateArticleRequest) returns (Article) {
    option (google.api.http) = {
      put: "/v1/{article.name=feeds/*/articles/*}"
      body: "*"
    };
  }

  rpc ReadArticle(ReadArticleRequest) returns (Article) {
    option (google.api.http) = {
      patch: "/v1/{name=feeds/*/articles/*}"
    };
  }

  rpc CrawlArticles(CrawlArticlesRequest) returns (CrawlArticlesResponse) {
    option (google.api.http) = {
      get: "/v1/{parent=feeds/*}/crawl"
    };
  }

  rpc ListUsers(ListUsersRequest) returns (ListUsersResponse) {
    option (google.api.http) = {
      get: "/v1/users"
    };
  }

  rpc GetUser(GetUserRequest) returns (User) {
    option (google.api.http) = {
      get: "/v1/{name=users/*}"
    };
  }

  rpc GetSelfUser(GetUserSelfRequest) returns (User) {
    option (google.api.http) = {
      post: "/v1/users/self"
      body: "*"
    };
  }

  rpc CreateUser(CreateUserRequest) returns (User) {
    option (google.api.http) = {
      post: "/v1/users"
      body: "*"
    };
  }

  rpc UpdateUser(UpdateUserRequest) returns (User) {
    option (google.api.http) = {
      put: "/v1/{user.name=users/*}"
      body: "*"
    };
  }

  rpc DeleteUser(DeleteUserRequest) returns (google.protobuf.Empty) {
    option (google.api.http) = {
      delete: "/v1/{name=users/*}"
    };
  }
}

message Feed {
  string name = 1;
  string UUID = 2;
  string slug = 3;
  string title = 4;
  string description = 5;
  string URL = 6;
  google.protobuf.Timestamp ts_subscribed = 7;
  bool is_public = 8;
}

enum Restrict {
  ALL = 0;
  PUBLIC = 1;
  MINE = 2;
}

message ListFeedsRequest {
  Restrict restrict = 1;
  int32 page_size = 2;
  string page_token = 3;
}

message ListFeedsResponse {
  repeated Feed feeds = 1;
  string next_page_token = 2;
}

message GetFeedRequest {
  string name = 1;
}

message CreateFeedRequest {
  Feed feed = 1;
}

message UpdateFeedRequest {
  Feed feed = 1;
  google.protobuf.FieldMask update_mask = 2;
}

message SubscribeFeedRequest {
  Feed feed = 1;
}

message UnsubscribeFeedRequest {
  Feed feed = 1;
}

message DeleteFeedRequest {
  string name = 1;
}

message Article {
  string name = 1;
  string UUID = 2;
  string slug = 3;
  google.protobuf.Timestamp timestamp = 4;
  string title = 5;
  string body = 6;
  string URL = 7;
  string checksum = 8;
  bool read = 9;
}

message ListArticlesRequest {
  string parent = 1;
  bool unread_only = 2;
  int32 page_size = 3;
  string page_token = 4;
  bool is_public = 5;
}

message ListArticlesResponse {
  repeated Article articles = 1;
  string next_page_token = 2;
}

message QueryArticlesRequest {
  repeated string feeds = 1;
  bool unread_only = 2;
  int32 page_size = 3;
  string page_token = 4;
  bool is_public = 5;
}

message QueryArticlesResponse {
  repeated Article articles = 1;
  string next_page_token = 2;
}

message GetArticleRequest {
  string name = 1;
}

message UpdateArticleRequest {
  Article article = 1;
}

message ReadArticleRequest {
  string name = 1;
}

message CrawlArticlesRequest {
  string parent = 1;
}

message CrawlArticlesResponse {
  repeated Article articles = 1;
}

message User {
  enum Role {
    UNKNOWN = 0;
    ADMIN = 1;
    USER = 2;
  }

  string name = 1;
  string UUID = 2;
  string email = 3;
  string configuration = 4;
  repeated Role role = 5;
  google.protobuf.Timestamp ts_lock = 6;
}

message ListUsersRequest {
  int32 page_size = 1;
  string page_token = 2;
}

message ListUsersResponse {
  repeated User users = 1;
  string next_page_token = 2;
}

message GetUserRequest {
  string name = 1;
}

message GetUserSelfRequest {
  string email = 1;
}

message CreateUserRequest {
  User user = 1;
}

message UpdateUserRequest {
  User user = 1;
  google.protobuf.FieldMask update_mask = 2;
}

message DeleteUserRequest {
  string name = 1;
}
