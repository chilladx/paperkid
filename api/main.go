package api

import (
	"encoding/base64"
	"fmt"
	"regexp"

	"github.com/google/uuid"
	"github.com/op/go-logging"
)

// Create a new instance of the logger
var log = logging.MustGetLogger("main")

// Standard names of the resources
const (
	ResourceNameFeed    string = "feeds"
	ResourceNameArticle string = "articles"
	ResourceNameUser    string = "users"
)

const uuidPattern = "([-_a-zA-Z0-9]{22})"

var feedRegexp = regexp.MustCompile(
	"^" +
		ResourceNameFeed + "/" + uuidPattern +
		"$",
)

// ParseNameFeed checks the validity of an Feed name and extracts the component UUIDs
func ParseNameFeed(name string) (feedUUID uuid.UUID, err error) {
	log.Debugf("Trying to decode: %s", name)
	matches := feedRegexp.FindStringSubmatch(name)
	if matches == nil {
		err = fmt.Errorf("invalid Feed name")
		return
	}
	feedUUID, err = Base64Decode(matches[1])
	if err != nil {
		err = fmt.Errorf("invalid Feed UUID: %s", err)
		return
	}
	return
}

var articleRegexp = regexp.MustCompile(
	"^" +
		ResourceNameFeed + "/" + uuidPattern +
		"/" + ResourceNameArticle + "/" + uuidPattern +
		"$",
)

// ParseNameArticle checks the validity of a Article name and extracts the component UUIDs
func ParseNameArticle(name string) (feedUUID, articleUUID uuid.UUID, err error) {
	matches := articleRegexp.FindStringSubmatch(name)
	if matches == nil {
		err = fmt.Errorf("invalid Article name")
		return
	}
	feedUUID, err = Base64Decode(matches[1])
	if err != nil {
		err = fmt.Errorf("invalid Feed UUID: %s", err)
		return
	}
	articleUUID, err = Base64Decode(matches[2])
	if err != nil {
		err = fmt.Errorf("invalid Article UUID: %s", err)
		return
	}
	return
}

// Base64Encode encodes a UUID to a url-safe base64 string with no padding.
func Base64Encode(u uuid.UUID) string {
	return base64.RawURLEncoding.EncodeToString(u[:])
}

// Base64Decode decodes a url-safe base64 string with no padding to a UUID.
func Base64Decode(s string) (u uuid.UUID, err error) {
	b, err := base64.RawURLEncoding.DecodeString(s)
	if err != nil {
		return
	}
	u, err = uuid.FromBytes(b)
	return
}

var userRegexp = regexp.MustCompile(
	"^" +
		ResourceNameUser + "/" + uuidPattern +
		"$",
)

// ParseNameUser checks the validity of an User name and extracts the component UUIDs
func ParseNameUser(name string) (userUUID uuid.UUID, err error) {
	log.Debugf("Trying to decode: %s", name)
	matches := userRegexp.FindStringSubmatch(name)
	if matches == nil {
		err = fmt.Errorf("invalid User name")
		return
	}
	userUUID, err = Base64Decode(matches[1])
	if err != nil {
		err = fmt.Errorf("invalid User UUID: %s", err)
		return
	}
	return
}
