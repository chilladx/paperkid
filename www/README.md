# Web UI

## Color palette

The color palette has been chosen after [Material System](https://material.io/design/color/the-color-system.html)

| Variant       | color   | Note            |
| ------------- | ------- | --------------- |
| Light Blue 50 | #E1F5FE |                 |
| 100           | #B3E5FC |                 |
| 200           | #81D4FA |                 |
| 300           | #4FC3F7 |                 |
| 400           | #29B6F6 |                 |
| 500           | #03A9F4 | Primary color   |
| 600           | #039BE5 |                 |
| 700           | #0288D1 |                 |
| 800           | #0277BD |                 |
| 900           | #01579B |                 |
| A100          | #80D8FF | Primary variant |
| A200          | #40C4FF |                 |
| A400          | #00B0FF |                 |
| A700          | #0091EA |                 |


| Variant  | color   | Note              |
| -------- | ------- | ----------------- |
| Amber 50 | #FFF8E1 |                   |
| 100      | #FFECB3 |                   |
| 200      | #FFE082 | Secondary color   |
| 300      | #FFD54F |                   |
| 400      | #FFCA28 |                   |
| 500      | #FFC107 |                   |
| 600      | #FFB300 |                   |
| 700      | #FFA000 |                   |
| 800      | #FF8F00 |                   |
| 900      | #FF6F00 |                   |
| A100     | #FFE57F |                   |
| A200     | #FFD740 | Secondary variant |
| A400     | #FFC400 |                   |
| A700     | #FFAB00 |                   |