'use strict'

module.exports ={
    API_BASE_URL: 'http://localhost:7778/v1',
    AUTH_BASE_URL: 'http://localhost:8765',
    NODE_ENV: '"development"'
}
