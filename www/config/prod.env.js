'use strict'

module.exports ={
    API_BASE_URL: JSON.stringify(process.env.API_BASE_URL),
    AUTH_BASE_URL: JSON.stringify(process.env.AUTH_BASE_URL),
    NODE_ENV: '"production"'
}
  