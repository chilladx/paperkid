import axios from 'axios';
import {getCookie} from 'tiny-cookie';
import Vue from 'vue';
import VueFlashMessage from 'vue-flash-message';
import ElementUI from 'element-ui';
// import 'element-theme-chalk'

import App from './app/App.vue';
import router from './app/router';
import store from './app/store';
import helpers from './helpers'

// Configure axios request Headers
const authToken = getCookie('paperkid-token');
if (authToken) {
  axios.defaults.headers.common['paperkid-token'] = authToken;
}

axios.defaults.baseURL = process.env.API_BASE_URL
axios.defaults.headers.post['Content-Type'] = 'application/json'
axios.defaults.headers.put['Content-Type'] = 'application/json'

// Configure axios response error interceptor
axios.interceptors.response.use(null, error => {
  if (error.response.status === 401) {
    v.flash(
        'There is a problem with your authentication, let\'s reconnect. (' +
            error.response.data.error + ')',
        'error', {timeout: 7000, pauseOnInteract: true});
    v.$store.dispatch('logoutUser');
    router.push({path: '/'});
  } else {
    v.flash(
        'There is a problem with the API: ' + error, 'error',
        {timeout: 3000, pauseOnInteract: true});
  }
});

// Configure messaging
require('vue-flash-message/dist/vue-flash-message.min.css');
Vue.use(VueFlashMessage, {createShortcuts: false});

Vue.prototype.$helpers = helpers;

Vue.use(ElementUI);

const v = new Vue({el: '#app', store, router, render: h => h(App)});
