import Vue from 'vue';
import VueRouter from 'vue-router';

import AdminUserLayout from '../components/layout/AdminUserLayout';
import FeedLayout from '../components/layout/FeedLayout';
import ReaderLayout from '../components/layout/ReaderLayout';
import NotFound from '../components/NotFound.vue';
import store from '../store'

Vue.use(VueRouter);

const ifAuthenticated =
    (to, from, next) => {
      next()
      if (store.getters.isAuthenticated) {
        next()
        return
      }
      next('/')
    }

const router = new VueRouter({
  mode: 'history',
  routes: [
    {path: '/', component: ReaderLayout},
    {
      path: '/admin/users',
      component: AdminUserLayout,
      beforeEnter: ifAuthenticated
    },
    {path: '/feeds/', component: FeedLayout},
    {path: '/admin/feeds', component: FeedLayout, beforeEnter: ifAuthenticated},
    {path: '*', component: NotFound}
  ]
});

export default router;