import axios from 'axios';
import * as KeratinAuthN from 'keratin-authn';
import {getCookie, isCookieEnabled, removeCookie, setCookie} from 'tiny-cookie';

import * as types from './mutation-types';

KeratinAuthN.setHost(process.env.AUTH_BASE_URL);
KeratinAuthN.setCookieStore('paperkid-token');

const state = {
  authSessionToken: KeratinAuthN.session() || '',
  authStatus: '',
  userUUID: getCookie('paperkid-UUID') || '',
  userConfiguration: {
    pageSize: getCookie('pageSize') || 20,
    sidebarVisible: getCookie('sidebarVisible') || true,
    onlyUnread: getCookie('onlyUnread') || false,
  },
  userRoles: getCookie('userRoles') || [],
};
const mutations = {
  [types.USER_REQUEST](state) {
    state.authStatus = 'loading';
  },
  [types.USER_SUCCESS](state, payload) {
    state.authStatus = 'success';
    state.userUUID = payload;
  },
  [types.USER_ERROR](state) {
    state.authStatus = 'user_error';
    state.userUUID = '';
  },
  [types.USER_LOGOUT](state) {
    state.authSessionToken = '';
    state.authStatus = '';
    state.userUUID = '';
    state.userConfiguration = {
      pageSize: 20,
      sidebarVisible: true,
      onlyUnread: false,  
    };
    state.userRoles = [];
  },
  [types.AUTH_READY](state) {
    state.authStatus = '';
  },
  [types.AUTH_REQUEST](state) {
    state.authStatus = 'authenticating';
  },
  [types.AUTH_SUCCESS](state, payload) {
    state.authStatus = 'success';
    state.authSessionToken = payload;
  },
  [types.AUTH_LOGOUT](state) {
    state.authStatus = 'logouting';
  },
  [types.AUTH_ERROR](state) {
    state.authStatus = 'auth_error';
    state.authSessionToken = '';
  },
  [types.USERCONF_PAGE_SIZE_UPDATE](state, payload) {
    state.userConfiguration.pageSize = payload;
  },
  [types.USERCONF_SIDEBAR_VISIBILITY_UPDATE](state, payload) {
    state.userConfiguration.sidebarVisible = payload;
  },
  [types.USERCONF_ONLY_UNREAD_UPDATE](state, payload) {
    state.userConfiguration.onlyUnread = payload;
  },
  [types.USER_ROLES_UPDATE](state, payload) {
    state.userRoles = payload;
  },
  [types.USER_UUID_UPDATE](state, payload) {
    state.userUUID = payload;
  }
};
const actions = {
  loadUser({commit}) {
    return new Promise((resolve, reject) => {
      let uuid = getCookie('paperkid-UUID') || '';
      commit(types.USER_UUID_UPDATE, uuid)
      let uConf = {
        pageSize: getCookie('pageSize') || 20,
        sidebarVisible: getCookie('sidebarVisible') || true,
        onlyUnread: getCookie('onlyUnread') || false,    
      };
      uConf.sidebarVisible = (uConf.sidebarVisible === true || uConf.sidebarVisible === 'true');
      uConf.onlyUnread = (uConf.onlyUnread === true || uConf.onlyUnread === 'true');
      commit(types.USERCONF_PAGE_SIZE_UPDATE, uConf.pageSize);
      commit(types.USERCONF_SIDEBAR_VISIBILITY_UPDATE, uConf.sidebarVisible);
      commit(types.USERCONF_ONLY_UNREAD_UPDATE, uConf.onlyUnread);
      let uRoles = getCookie('userRoles') || [];
      commit(types.USER_ROLES_UPDATE, uRoles);
      resolve();
    })
  },
  createUser({state, commit}, userItem) {
    return new Promise((resolve, reject) => {
      commit(types.AUTH_REQUEST);
      KeratinAuthN
          .signup({username: userItem.login, password: userItem.password})
          .then((response) => {
            let authSessionToken = KeratinAuthN.session();
            if (authSessionToken != '') {
              commit(types.AUTH_SUCCESS, authSessionToken);
              axios.defaults.headers.common['paperkid-token'] =
                  authSessionToken;
            };

            commit(types.USER_REQUEST);
            axios
                .post('/users', JSON.stringify({
                  user: {
                    email: userItem.login,
                    configuration: JSON.stringify(state.userConfiguration)
                  }
                }))
                .then((response) => {
                  setCookie('paperkid-UUID', response.data.UUID);
                  commit(types.USER_SUCCESS, response.data.UUID);
                  resolve(response);
                })
                .catch((err) => {
                  commit(types.USER_ERROR);
                  removeCookie('paperkid-UUID');
                  reject(err);
                });
          })
          .catch((err) => {
            commit(types.AUTH_ERROR);
            delete axios.defaults.headers.common['paperkid-token'];
            reject(err);
          });
    })
  },
  updateUser({commit}, userUpdate) {
    var updateData = {user: {}, update_mask: {paths: []}};
    userUpdate.fields.forEach(function(field) {
      updateData.update_mask.paths.push(field);
      switch (field) {
        case 'configuration':
          updateData.user.configuration =
              JSON.stringify(userUpdate.userItem.configuration);
          break;
        case 'role':
          updateData.user.role = userUpdate.userItem.role;
          break;
        case 'ts_lock':
          updateData.user.ts_lock = userUpdate.userItem.ts_lock;
          break;
      }
    })
    return new Promise((resolve, reject) => {
      if (userUpdate.userItem.UUID !== '') {
        commit(types.USER_REQUEST);
        axios
            .put(
                '/users/' + userUpdate.userItem.UUID,
                JSON.stringify(updateData))
            .then((response) => {
              resolve(response);
            })
            .catch((err) => {
              reject(err);
            });
        } else {
          resolve();
        }
    })
  },
  loginUser({commit, dispatch}, userItem) {
    return new Promise((resolve, reject) => {
      commit(types.AUTH_REQUEST);
      KeratinAuthN
          .login({username: userItem.login, password: userItem.password})
          .then((response) => {
            let authSessionToken = KeratinAuthN.session();
            if (authSessionToken != '') {
              commit(types.AUTH_SUCCESS, authSessionToken);
              axios.defaults.headers.common['paperkid-token'] =
                  authSessionToken;
            };
            commit(types.USER_REQUEST);
            axios
                .post(
                    '/users/self',
                    JSON.stringify({email: userItem.login}))
                .then((response) => {
                  setCookie('paperkid-UUID', response.data.UUID);
                  commit(types.USER_SUCCESS, response.data.UUID);
                  dispatch('loadUserRoles', response.data.role);
                  dispatch(
                      'loadUserConf', JSON.parse(response.data.configuration));
                  resolve();
                })
                .catch((err) => {
                  commit(types.USER_ERROR);
                  removeCookie('paperkid-UUID');
                  reject(err);
                });
            resolve();
          })
          .catch((err) => {
            commit(types.AUTH_ERROR);
            removeCookie('paperkid-token');
            delete axios.defaults.headers.common['paperkid-token'];
            reject(err);
          });
    })
  },
  logoutUser({commit}) {
    return new Promise((resolve, reject) => {
      commit(types.AUTH_LOGOUT);
      KeratinAuthN.logout()
          .then((response) => {
            commit(types.AUTH_SUCCESS, '');
            delete axios.defaults.headers.common['paperkid-token'];
            commit(types.AUTH_READY);
            commit(types.USER_LOGOUT);
            //removeCookie('paperkid-UUID');
            removeCookie('userRoles');
            resolve(response);
          })
          .catch((err) => {
            commit(types.AUTH_ERROR);
            reject(err);
          });
    })
  },
  deleteUser({}, userItem) {
    return new Promise((resolve, reject) => {
      axios.delete('/' + userItem.name)
          .then((response) => {
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
    })
  },
  loadUserRoles({commit}, userRoles) {
    setCookie('userRoles', userRoles);
    commit(types.USER_ROLES_UPDATE, userRoles);
  },
  loadUserConf({commit}, userConf) {
    if (userConf.hasOwnProperty('pageSize')) {
      setCookie('pageSize', userConf.pageSize);
      commit(types.USERCONF_PAGE_SIZE_UPDATE, userConf.pageSize);
    }
    if (userConf.hasOwnProperty('onlyUnread')) {
      setCookie('onlyUnread', userConf.onlyUnread);
      commit(types.USERCONF_ONLY_UNREAD_UPDATE, userConf.onlyUnread);
    }
    if (userConf.hasOwnProperty('sidebarVisible')) {
      setCookie('sidebarVisible', userConf.sidebarVisible);
      commit(types.USERCONF_SIDEBAR_VISIBILITY_UPDATE, userConf.sidebarVisible);
    }
  },
  updateUserConfPageSize({state, getters, commit, dispatch}, newValue) {
    setCookie('pageSize', newValue);
    commit(types.USERCONF_PAGE_SIZE_UPDATE, newValue);
    if (getters.isAuthenticated) {
      dispatch('updateUser', {
        userItem: {UUID: state.userUUID, configuration: state.userConfiguration},
        fields: ['configuration']
      });
    }
  },
  toggleUserConfOnlyUnread({state, getters, commit, dispatch}) {
    setCookie('onlyUnread', !state.userConfiguration.onlyUnread);
    commit(
        types.USERCONF_ONLY_UNREAD_UPDATE, !state.userConfiguration.onlyUnread);
    if (getters.isAuthenticated) {
      dispatch('updateUser', {
        userItem: {UUID: state.userUUID, configuration: state.userConfiguration},
        fields: ['configuration']
      });
    }
  },
  toggleUserConfSidebar({state, getters, commit, dispatch}) {
    setCookie('sidebarVisible', !state.userConfiguration.sidebarVisible);
    commit(
        types.USERCONF_SIDEBAR_VISIBILITY_UPDATE,
        !state.userConfiguration.sidebarVisible);
    if (getters.isAuthenticated) {
      dispatch('updateUser', {
        userItem: {UUID: state.userUUID, configuration: state.userConfiguration},
        fields: ['configuration']
      });
    }
  },
};
const getters = {
  authSessionToken: state => state.authSessionToken,
  authStatus: state => state.authStatus,
  userUUID: state => state.userUUID,
  isValidUser: state => {
    return state.userUUID != ''
  },
  isAuthenticated: state => {
    return state.authSessionToken != ''
  },
  pageSize: state => state.userConfiguration.pageSize,
  sidebarVisible: state => state.userConfiguration.sidebarVisible,
  onlyUnread: state => state.userConfiguration.onlyUnread,
  hasRoleUser: state => {
    if (state.userRoles === undefined || state.userRoles.length == 0) {
      return false;
    } else {
      return (state.userRoles.indexOf('USER') > -1);
    }
  },
  hasRoleAdmin: state => {
    if (state.userRoles === undefined || state.userRoles.length == 0) {
      return false;
    } else {
      return (state.userRoles.indexOf('ADMIN') > -1);
    }
  }
};

const userModule = {
  state,
  mutations,
  actions,
  getters
}

export default userModule;