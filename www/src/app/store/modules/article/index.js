import axios from 'axios';
import * as types from './mutation-types';

const state = {
  articleItems: [],
  nextPageToken: '',
};
const mutations = {
  [types.UPDATE_ARTICLE_ITEMS](state, payload) {
    state.articleItems = payload;
  },
  [types.APPEND_ARTICLE_ITEMS](state, payload) {
    state.articleItems = state.articleItems.concat(payload);
  },
  [types.UPDATE_ARTICLE_ITEM](state, payload) {
    state.articleItems.forEach(function(articleItem, articleIdx) {
      if (articleItem.UUID === payload.UUID) {
        state.articleItems[articleIdx] = payload;
      }
    });
  },
  [types.UDPATE_ARTICLE_NEXTPAGETOKEN](state, payload) {
    state.nextPageToken = payload;
  }
};
const actions = {
  getArticleItems({commit, rootState}, is_public) {
    var params = {
      page_size: rootState.user.userConfiguration.pageSize
    }

    if (rootState.user.userConfiguration.onlyUnread) {
      params.unread_only = true;
    }
    if (is_public) {
      params.is_public = true;
    }
    if (rootState.feed.feedSelected !== undefined && rootState.feed.feedSelected.length > 0) {
      params.feeds = rootState.feed.feedSelected;
    }

    axios.post('/articles', JSON.stringify(params))
        .then((response) => {
          commit(
              types.UDPATE_ARTICLE_NEXTPAGETOKEN,
              response.data.next_page_token);
          commit(types.UPDATE_ARTICLE_ITEMS, response.data.articles)
        })
        .catch((err) => {
          commit(types.UDPATE_ARTICLE_NEXTPAGETOKEN, '');
        });
  },
  appendArticleItems({state, commit, rootState}, is_public, feeds) {
    var params = {
      page_size: rootState.user.userConfiguration.pageSize
    }

    if (rootState.user.userConfiguration.onlyUnread) {
      params.unread_only = true;
    }
    if (is_public) {
      params.is_public = true;
    }
    if (feeds !== undefined && feeds.length > 0) {
      params.feeds = feeds;
    }

    params.page_token = state.nextPageToken;
    
    axios.post('/articles', JSON.stringify(params))
        .then((response) => {
          commit(
              types.UDPATE_ARTICLE_NEXTPAGETOKEN,
              response.data.next_page_token);
          commit(types.APPEND_ARTICLE_ITEMS, response.data.articles)
        })
        .catch((err) => {
          commit(types.UDPATE_ARTICLE_NEXTPAGETOKEN, '');
        });
  },
  readArticle({state, commit}, article) {
    axios.patch('/' + article.name)
        .then((response) => {
          commit(types.UPDATE_ARTICLE_ITEM, response.data);
        })
        .catch((error) => console.log(error))
  }
};
const getters = {
  articleItems: state => state.articleItems,
  nextPageToken: state => state.nextPageToken,
};

const articleModule = {
  state,
  mutations,
  actions,
  getters
}

export default articleModule;