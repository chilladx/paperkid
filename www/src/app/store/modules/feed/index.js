import axios from 'axios';
import * as types from './mutation-types';

const restrict = {
  ALL: 0,
  PUBLIC: 1,
  MINE: 2
};

const state = {
  feedItems: [],
  nextPageToken: '',
  feedSelected: [],
};
const mutations = {
  [types.INIT_FEED_ITEMS](state, payload) {
    state.feedItems = payload;
  },
  [types.UPDATE_FEED_ITEMS](state, payload) {
    state.feedItems = state.feedItems.concat(payload);
  },
  [types.UPDATE_NEXT_PAGE_TOKEN](state, payload) {
    state.nextPageToken = payload;
  },
  [types.UDPATE_FEED_SELECTED](state, payload) {
    state.feedSelected = payload;
  },
  [types.APPEND_FEED_ITEM](state, payload) {
    if (state.feedItems !== undefined) {
      state.feedItems.push(payload);
    } else {
      state.feedItems = [payload];
    }
  },
  [types.DELETE_FEED_ITEM](state, payload) {
    const feedItemIndex =
        state.feedItems.findIndex(feedItem => feedItem.UUID === payload.UUID);
    state.feedItems.splice(feedItemIndex, 1);
  },
  [types.APPEND_FEED_SELECTION](state, payload) {
    if (state.feedSelected !== undefined) {
      state.feedSelected.push(payload);
    } else {
      state.feedSelected = [payload];
    }
  },
  [types.REMOVE_FEED_SELECTION](state, payload) {
    const feedSelectedIndex =
        state.feedSelected.findIndex(feedItem => feedItem === payload);
    if (feedSelectedIndex > -1) {
      state.feedSelected.splice(feedSelectedIndex, 1);

    }
  },
};
const actions = {
  resetFeedItem({commit}) {
    return new Promise((resolve, reject) => {
      commit(types.INIT_FEED_ITEMS, []);
      resolve();
    })
  },
  loadFeedItem({state, commit}, feedUUID) {
    return new Promise((resolve, reject) => {
      axios.get('/feeds/' + feedUUID)
          .then((response) => {
            resolve(response);
          })
          .catch((err) => {reject(err)});
    })
  },
  getMyFeedItems({commit}, token) {
    var url = '/feeds?page_size=20&restrict='+ restrict.MINE;
    if (token !== undefined && token !== '') {
      url += '&page_token=' + token;
    }
    axios.get(url)
        .then((response) => {
          commit(types.UPDATE_FEED_ITEMS, response.data.feeds);
          commit(types.UPDATE_NEXT_PAGE_TOKEN, response.data.next_page_token);
        })
        .catch((err) => {commit(types.INIT_FEED_ITEMS, [])});
  },
  getPublicFeedItems({commit}, token) {
    var url = '/feeds?page_size=20&restrict='+ restrict.PUBLIC;
    if (token !== undefined && token !== '') {
      url += '&page_token=' + token;
    }
    axios.get(url)
        .then((response) => {
          commit(types.UPDATE_FEED_ITEMS, response.data.feeds);
          commit(types.UPDATE_NEXT_PAGE_TOKEN, response.data.next_page_token);
        })
        .catch((err) => {commit(types.INIT_FEED_ITEMS, [])});
  },
  getFeedItems({commit}, token) {
    var url = '/feeds?page_size=20&restrict='+ restrict.ALL;
    if (token !== undefined && token !== '') {
      url += '&page_token=' + token;
    }
    return axios.get(url);
  },
  toggleFeedSelection({getters, commit}, feedItem) {
    if (getters.isSelectedFeed(feedItem)) {
      commit(types.REMOVE_FEED_SELECTION, feedItem.name);
    } else {
      commit(types.APPEND_FEED_SELECTION, feedItem.name);
    }
  },
  resetFeedSelection({commit}) {
    commit(types.UDPATE_FEED_SELECTED, []);
  },
  deleteFeed({state, commit}, feedItem) {
    return axios.delete('/' + feedItem.name);
  },
  subscribeFeed({state, commit}, feedItem) {
    return new Promise((resolve, reject) => {
      axios.post('/' + feedItem.name + '/subscribe')
          .then((response) => {
            commit(types.APPEND_FEED_ITEM, response.data);
            resolve(response);
          })
          .catch((err) => {reject(err)});
    })
  },
  unsubscribeFeed({state, commit}, feedItem) {
    return new Promise((resolve, reject) => {
      axios.post('/' + feedItem.name + '/unsubscribe')
          .then((response) => {
            commit(types.DELETE_FEED_ITEM, feedItem);
            resolve(response);
          })
          .catch((err) => {reject(err)});
    })
  },
  createFeedItem({commit}, feedItem) {
    return new Promise((resolve, reject) => {
      axios
          .post('/feeds', JSON.stringify({
            feed: {
              title: feedItem.title,
              URL: feedItem.URL,
              description: feedItem.description,
              is_public: feedItem.is_public
            }
          }))
          .then((response) => {
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
    })
  },
  updateFeedItem({commit}, feedItem) {
    return new Promise((resolve, reject) => {
      axios
          .put('/' + feedItem.name, JSON.stringify({
            feed: {
              title: feedItem.title,
              URL: feedItem.URL,
              description: feedItem.description,
              is_public: feedItem.is_public
            },
            update_mask: {
              paths: ['title', 'URL', 'description', 'IsPublic'],
            }
          }))
          .then((response) => {
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
    })
  },
};
const getters = {
  feedItems: state => state.feedItems,
  nptFeedsMenu: state => state.nextPageToken,
  feedSelected: state => state.feedSelected,
  isSubscribedFeed: (state) => (feed) => {
    if (state.feedItems !== undefined) {
      return (
          state.feedItems.find(feedItem => feedItem.name === feed.name) !==
          undefined)
    } else {
      return false;
    }
  },
  isSelectedFeed: (state) => (feed) => {
    return (
      state.feedSelected.find(feedItem => feedItem === feed.name) !== undefined)
  },
  feedSelection: state => state.feedSelected.length > 0 ? state.feedSelected : state.feedItems
};

const feedModule = {
  state,
  mutations,
  actions,
  getters
}

export default feedModule;